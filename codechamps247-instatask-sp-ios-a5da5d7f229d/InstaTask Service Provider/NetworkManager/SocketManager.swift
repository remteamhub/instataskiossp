//
//  SocketManager.swift
//  InstaTask
//
//  Created by Arqam Butt on 16/11/2019.
//  Copyright © 2019 CodeChamps. All rights reserved.
//

import Foundation
import SocketIO

enum SocketName: Int {
    case status = 0
    case getDriversData = 1
}

protocol SocketStatusDelegate: class {
    func socketEventStatus(event: SocketClientEvent, data: [Any], ack: SocketAckEmitter)
    func didReceiveResponseOfApi(api: SocketName, dataArray:Any)
}

typealias success = (_ status: String, _ data: Any) -> Void
typealias failure = (_ error: NSError) -> Void

class SocketManagerSingeleton: NSObject {
    
    static let shared: SocketManagerSingeleton = {
        let instance = SocketManagerSingeleton()
        return instance
    }()
    
    weak var delegate: SocketStatusDelegate?
    
//    fileprivate let manager = SocketManager(socketURL: URL(string: "http://134.209.115.28:3000")! , config: [.log(true), .compress])
//    fileprivate var socket: SocketIOClient!
    
    fileprivate let manager = SocketManager(socketURL: URL(string: socketURL)!, config: [.log(true), .compress, .connectParams([
        "userID" : "\(Users.shared.id)"
    ])])
//    fileprivate lazy var socket: SocketIOClient = {
//        let instance: SocketIOClient!
//        return instance
//    }()
    
    fileprivate var socket: SocketIOClient?
    
//    private func sendResponseOfApi(api: SocketName, dataObj:Any, delegate: SocketStatusDelegate) {
//        self.delegate?.didReceiveResponseOfApi(api: api, dataArray: dataObj)
//    }
    
//    func getManager() -> SocketManager {
//        return manager
//    }
    
    func getSocket() -> SocketIOClient? {
        socket = self.manager.defaultSocket
        return socket
    }
    
//    func checkConnectEvent(delegate: SocketStatusDelegate){
////        self.socket.on(clientEvent: .connect) { [weak self] (data, ack) in
////            delegate.socketEventStatus(event: .connect, data: data, ack: ack)
////        }
//    }
    
    func setupSocketManager(delegate: SocketStatusDelegate){
        
        socket = getSocket()
        
        //Events Start
        //-------------------------------------
        
        self.socket?.on(clientEvent: .connect) {data, ack in
//            print("\n socket connected \n")
            delegate.socketEventStatus(event: .connect, data: data, ack: ack)
        }
            
        self.socket?.on(clientEvent: .disconnect) {data, ack in
//            print("\n socket disconnected \n")
            delegate.socketEventStatus(event: .disconnect, data: data, ack: ack)
        }
        
        self.socket?.on(clientEvent: .error) { (data, ack) in
//            print("\n ================\n \(data) \n \(ack) \n=============\n")
            delegate.socketEventStatus(event: .error, data: data, ack: ack)
        }
        
        //Events Close
        //-------------------------------------
        
        //Status Start
        //-------------------------------------
            
        self.socket?.on("status") { (data, ack) in
//            print("\n **************** \n",data, ack, "\n **z************** \n")
            delegate.didReceiveResponseOfApi(api: SocketName.status, dataArray: data)
        }
            
        self.socket?.on("getDriversData", callback: { (data, ack) in
//            print("\n @@@@@@@@@@@@@@@@ \n",data, ack, "\n @@@@@@@@@@@@@@@@ \n")
           delegate.didReceiveResponseOfApi(api: SocketName.getDriversData, dataArray: data)
//                dispatchSocketGroup.leave()
        })
        
        //Status Close
        //-------------------------------------
        self.socket?.connect()
    }
 
    func connectToSocket() {
        self.setupSocketManager(delegate: HomeViewController())
    }
    
    func disconectSocket() {
        self.getSocket()!.disconnect()
    }
    
}
