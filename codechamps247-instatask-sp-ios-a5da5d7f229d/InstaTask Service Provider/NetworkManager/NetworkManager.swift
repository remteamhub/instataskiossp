//
//  NetworkManager.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/20/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Stripe
import Alamofire


@objc enum ApiName: Int {
    
    case eApiLogin = 0
    case eApiLogout = 1
    case eApiSignUp = 2
    case eApiSocialLogin = 3
    case eApiUpdateLicense = 4
    case eApiCheckCurrentJobs = 5
    case eApiCheckJobById = 6
    case eApiUpdateWorkStatus = 7
    case eApiAcceptJob = 8
    case eApiCancelJob = 9
    case eApiUpdateStatus = 10
    case eApiRequestJobApproval = 11
    case eApiUpdateLatLong = 12
    case eApiLimitations = 13
    case eApiCustomerRating = 14
    case eApiCheckPendingJobs = 15
    case eApiCheckPreviousJobs = 16
    case eApiJobEditRequest = 17
    case eApiRejectJobRequest = 18
    case eApiGetChat = 19
    case eApiSendChat = 20
    case eApiPaginate  = 21
    case eApiCreatIssue = 22
    case eApiAddBankInfo = 23
    case eApiGetBankInfo = 24
    case eApiForgetPassword = 25
    case eApiGetFpCode = 26
    case eApiUpdatePassword = 27
    case eApiWeeklyEarning = 28
    case eApiUpdatePhone = 29
    
}

@objc protocol NetworkManagerDelegate:class {
    @objc optional func didReceiveResponseOfApi(api: ApiName, dataArray:Any)
    @objc optional func didReceiveResponseOfApi(api: ApiName, error:NSError)
}

class NetworkManager: NSObject
{
    var token: String!
    weak var delegate : NetworkManagerDelegate?
    
//    static let shared: NetworkManager = {
//        let instance = NetworkManager()
//        return instance
//    }()
    
    class private func sendResponseOfApiWithError(api: ApiName, error:NSError, delegate: NetworkManagerDelegate) {
        delegate.didReceiveResponseOfApi!(api: api, error: error)
    }
    
    class private func sendResponseOfApi(api: ApiName, dataObj:Any, delegate: NetworkManagerDelegate) {
        delegate.didReceiveResponseOfApi!(api: api, dataArray: dataObj)
    }
    
    
    class private func getRequest(url:String, params:[String:Any]?, showHUD showHud: Bool, success: @escaping (_ status: String, _ data: Any) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        
        if Utilities.isNetworkReachable() {
            if showHud {
                Utilities.showHUD()
            }
            
            var rurl = "\(baseURL)\(url)"
            print("URL: \(rurl)")
            
            if let p = params{
                if p.count > 0{
                    rurl += "?"
                    for (key, value) in p{
                        rurl += "\(key)=\(value as! String)&"
                    }
                    rurl = String(rurl.dropLast())
                }
                
            }
        
            //let original = "http://www.geonames.org/search.html?q=Aïn+Béïda+Algeria&country="
            if let encoded = rurl.addingPercentEncoding(withAllowedCharacters: .urlFragmentAllowed),
                let url = URL(string: encoded)
            {
                AF.request("\(url)", method: .get, parameters: nil, encoding: JSONEncoding.default, headers: nil).responseJSON { response in
                    
                    Utilities.hideHUD()
                    
                    if (response.response != nil){
                        
                        switch response.result {
                        case .success(let value):
                            let resp = value as! Dictionary<String, Any>

                            Utilities.printPrettyJSONFromDic(params: resp)
                            
                            success("success", resp as Any)
                        case .failure(let error):
                            failure(error as NSError)
                        }
                    }else{
                        print("\n-------- Get Else --------\n")

                    }
                    
                }
            }
            
            
        }
        
    }
    
    
//    class private func deleteRequestWithURL(url:String, params:[String:Any], showHUD showHud: Bool, success: @escaping (_ status: String, _ data: Any) -> Void, failure: @escaping (_ error: NSError) -> Void) {
//
//        if Utilities.isNetworkReachable() {
//            if showHud {
//                Utilities.showHUD()
//            }
//
//            let rurl = "\(baseURL)\(url)"
//            print("URL: \(rurl)")
//            print("Params: \(params)")
//
//            AF.request("\(rurl)", method: .delete, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
//
//                Utilities.hideHUD()
//
//                if (response.response != nil){
//
//                    let resp = response.result.value as! Dictionary<String, Any>
//                    let status = resp["status"] as! String
//
//                    Utilities.printPrettyJSONFromDic(params: resp)
//
//                    if status == "true"{
//                        success("success", resp as Any)
//
//                    }else {
//                        let err = resp["message"] as! String
//                        failure(Utilities.generateError(message: err))
//                    }
//                }else{
//                    if let error = response.result.error {
//                        failure(error as NSError)
//                    }
//                }
//
//
//            }
//        }
//    }
    
    
    class private func postRequestWithURL(url:String, params:[String:Any], showHUD showHud: Bool, success: @escaping (_ status: String, _ data: Any) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        
        if Utilities.isNetworkReachable() {
            if showHud {
                Utilities.showHUD()
            }
            
            let rurl = "\(baseURL)\(url)"
            print("URL: \(rurl)")
            print("Params: \(params)")
            
            AF.request("\(rurl)", method: .post, parameters: params, encoding: JSONEncoding.default).responseJSON { (response) in
                
                Utilities.hideHUD()
                
                if (response.response != nil){
                    
                    switch response.result {
                    case .success(let value):
                        print(value)
                        
                        let resp = value as! Dictionary<String, Any>
                        let status = resp["code"] as! Int
                        Utilities.printPrettyJSONFromDic(params: resp)
                        
                        if status == 200 {
                            success("success", resp["data"] as Any)
                            
                        }else {
                            let err = resp["error_msg"] as! String
                            failure(Utilities.generateError(message: err))
                        }
                    case .failure(let error):
                        failure(error as NSError)
                    }

                }else{
                    print("\n-------- POST Else --------\n")

                }
                
            }
        }
    }
    
    
    class private func formDataPostRequest(url:String, params:[String:Any], file:URL? = nil, insurance:URL? = nil, imageKey: String = "", showHUD showHud: Bool, success: @escaping (_ status: String, _ data: Any) -> Void, failure: @escaping (_ error: NSError) -> Void) {
        
        if Utilities.isNetworkReachable() {
            if showHud {
                Utilities.showHUD()
            }
            
            let rurl = "\(baseURL)\(url)"
            print("URL: \(rurl)")
            print("Params: \(params)")
            let header: HTTPHeaders = ["Content-Type": "application/x-www-form-urlencoded"]
            
            AF.upload(multipartFormData: { (formdata) in
                for (key,value) in params{
                    formdata.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
                    }

                if let u = file{

                    do {
                       let imageData = try Data(contentsOf: u)
                       let image = UIImage.init(data: imageData)!
                       
                       formdata.append(image.jpegData(compressionQuality: 0.2)!, withName: imageKey, fileName: "image.jpg", mimeType: "image/jpeg")
                    } catch {
                       print("Unable to load data: \(error)")
                    }
                }

                if let u = insurance{

                    do {
                       let imageData = try Data(contentsOf: u)
                       let image = UIImage.init(data: imageData)!
                       
                       formdata.append(image.jpegData(compressionQuality: 0.2)!, withName: "insurance_img", fileName: "insurance.jpg", mimeType: "image/jpeg")
                    } catch {
                       print("Unable to load data: \(error)")
                    }
                }
            }, to: rurl, headers: header).responseJSON { (response) in
                
                
                Utilities.hideHUD()
              
                if (response.response != nil){
                   
                    switch response.result {
                    case .success(let value):
                        print(value)
                        
                        let resp = value as! Dictionary<String, Any>
                        let status = resp["code"] as! Int
                        Utilities.printPrettyJSONFromDic(params: resp)
                        
                        if status == 200 {
                            success("success", resp["data"] as Any)
                            
                        }else {
                            var errMsg = String()
                            if let errDic = resp["error_msg"] as? NSDictionary {
                                if let username = errDic["username"] as? NSArray {
                                    errMsg += "\(username[0] as! String) \n"
                                }
                                if let email = errDic["email"] as? NSArray {
                                    errMsg += "\(email[0] as! String) \n"
                                }
                                if let phone = errDic["phone"] as? NSArray {
                                    errMsg += "\(phone[0] as! String) \n"
                                }
                                if let fcm_token = errDic["fcm_token"] as? NSArray {
                                    errMsg += "\(fcm_token[0] as! String) \n"
                                }
                            }else if let errDic = resp["error_msg"] as? String{
                                errMsg += errDic
                            }
                            
                            failure(Utilities.generateError(message: errMsg))
                        }
                    case .failure(let error):
                        failure(error as NSError)
                    }
        
                }else{
                    print("\n-------- FORM DATA POST POST Else --------\n")
                }
            }
            
        }
    }
    
//    class private func multipartRequest(url:String, params:[String:Any], file:URL?,showHUD showHud: Bool, success: @escaping (_ status: String, _ data: Any) -> Void, failure: @escaping (_ error: NSError) -> Void) {
//
//        if Utilities.isNetworkReachable() {
//            if showHud {
//                Utilities.showHUD()
//            }
//
//            let rurl = "\(baseURL)\(url)"
//            print("URL: \(rurl)")
//            print("Params: \(params)")
//
////            let header = [Constants.LangHeaderKey: Localize.currentLanguage(),
////                          "Content-Type":"multipart/form-data; boundary=MultipartBoundry"]
//
//            Alamofire.upload(multipartFormData: { (formdata) in
//
//                for (key,value) in params{
//                    formdata.append((value as AnyObject).data(using: String.Encoding.utf8.rawValue)!, withName: key)
//                }
//
//                if let u = file{
//
//                    do {
//                        let imageData = try Data(contentsOf: u)
//                        let image = UIImage.init(data: imageData)!
//
//                        formdata.append(image.jpegData(compressionQuality: 0.7)!, withName: "image", fileName: "image.jpg", mimeType: "image/jpeg")
//                    } catch {
//                        print("Unable to load data: \(error)")
//                    }
//                }
//
//            },to: rurl, headers: nil,encodingCompletion: { (result) in
//
//                switch result {
//
//                case .success(let request, _, _):
//
//                    request.responseJSON(completionHandler: { (response) in
//                        Utilities.hideHUD()
//                        if (response.response != nil){
//
//                            guard let r = response.result.value  else {return}
//                            let resp = r as! Dictionary<String, Any>
//                            let status = resp["status"] as! String
//
//                            Utilities.printPrettyJSONFromDic(params: resp)
//
//                            if status == "true"{
//                                success("success", resp as Any)
//
//                            }else {
//                                if let err = resp["message"] as? String {
//                                    failure(Utilities.generateError(message: err))
//                                }
//                                if let err = resp["error"] as? String {
//                                    failure(Utilities.generateError(message: err))
//                                }
//                            }
//                        }else{
//                            if let error = response.result.error {
//                                failure(error as NSError)
//                            }
//                        }
//
//                    })
//                case .failure(_):
//                    Utilities.hideHUD()
//                    break
//                }
//            })
//
//
//        }
//    }
    
    
    
    class func loginApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/login", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiLogin, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiLogin, error: error, delegate:delegate)
        }
    }
    
    class func logoutApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/logout", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiLogout, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiLogout, error: error, delegate:delegate)
        }
    }
    
    class func signUpApi(params: [String:Any], file: URL, insurance: URL?, delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "provider/register", params: params, file: file, insurance: insurance, imageKey: "license_img", showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiSignUp, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiSignUp, error: error, delegate:delegate)
        }
    }
    
    class func socialLoginApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "provider/socialLogin", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiSocialLogin, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiSocialLogin, error: error, delegate:delegate)
        }
    }
    
    class func updateLicenseApi(params: [String:Any], file: URL?, insurance: URL?,
                                image: String?, delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "provider/updateProfile", params: params, file: file, insurance: insurance, imageKey: image!, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiUpdateLicense, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdateLicense, error: error, delegate:delegate)
        }
    }
    
    class func updatePhoneApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "provider/updateProfile", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiUpdatePhone, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdatePhone, error: error, delegate:delegate)
        }
    }
    
    class func checkCurrentJobsApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        DispatchQueue.main.async {
            self.postRequestWithURL(url: "jobs/checkCurrentJob", params: params, showHUD: showHUD, success: { (status, data) in
                if let data = data as? [String: AnyObject] {
                    self.sendResponseOfApi(api: ApiName.eApiCheckCurrentJobs, dataObj:data, delegate: delegate)
                }else{
                    self.sendResponseOfApi(api: ApiName.eApiCheckCurrentJobs, dataObj:data as! NSArray, delegate: delegate)
                }
                dispatchGroup.leave()
            }) { (error) in
                    self.sendResponseOfApiWithError(api: ApiName.eApiCheckCurrentJobs, error: error, delegate:delegate)
                    dispatchGroup.leave()
            }
        }
        
    }
    
    class func checkPreviousJobsApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/checkPreviousJob", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiCheckPreviousJobs, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiCheckPreviousJobs, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCheckPreviousJobs, error: error, delegate:delegate)
        }
    }
    
    class func checkPendingJobsApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/checkPendingJob", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiCheckPendingJobs, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiCheckPendingJobs, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCheckPendingJobs, error: error, delegate:delegate)
        }
    }
    
    class func checkJobByIdApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/checkJobById", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiCheckJobById, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCheckJobById, error: error, delegate:delegate)
        }
    }
    
    class func updateWorkStatusApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/updateWorkStatus", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiUpdateWorkStatus, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdateWorkStatus, error: error, delegate:delegate)
        }
    }
    
    class func acceptJobApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/acceptJob", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiAcceptJob, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiAcceptJob, error: error, delegate:delegate)
        }
    }
    
    class func limitationsApi(delegate:NetworkManagerDelegate, showHUD: Bool){
        self.getRequest(url: "general/limitations", params: nil, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiLimitations, dataObj:data as! [String: AnyObject], delegate: delegate)
            dispatchGroup.leave()
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiLimitations, error: error, delegate:delegate)
            dispatchGroup.leave()
        }
    }

    class func cancelJobApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/leftJob", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiCancelJob, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCancelJob, error: error, delegate:delegate)
        }
    }
    
    class func updateStatusApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/updateStatus", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiUpdateStatus, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdateStatus, error: error, delegate:delegate)
        }
    }
    
    class func updateStatusWihtImageApi(params: [String:Any], file: URL, imageKey: String, delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "jobs/updateStatus", params: params, file: file, imageKey: imageKey, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiUpdateStatus, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdateStatus, error: error, delegate:delegate)
        }
    }
    
    class func requestJobApprovalApi(params: [String:Any], file: URL, imageKey: String, delegate:NetworkManagerDelegate, showHUD: Bool){
        self.formDataPostRequest(url: "provider/requestJobApproval", params: params, file: file, imageKey: imageKey, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiRequestJobApproval, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiRequestJobApproval, error: error, delegate:delegate)
        }
    }
    
    class func updateLatLongApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        DispatchQueue.main.async {
            self.postRequestWithURL(url: "provider/updateLatLng", params: params, showHUD: showHUD, success: { (status, data) in
                self.sendResponseOfApi(api: ApiName.eApiUpdateLatLong, dataObj:data as! [String: AnyObject], delegate: delegate)
                dispatchGroup.leave()
            }) { (error) in
                self.sendResponseOfApiWithError(api: ApiName.eApiUpdateLatLong, error: error, delegate:delegate)
                dispatchGroup.leave()
            }
        }
    }
    
    class func jobEditRequestApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/editRequestStatus", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiJobEditRequest, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiJobEditRequest, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiJobEditRequest, error: error, delegate:delegate)
        }
    }
    
    class func customerRatingApi(params: [String:Any],delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/rating", params: params, showHUD: showHUD, success: { (status, data) in
            
            self.sendResponseOfApi(api: ApiName.eApiCustomerRating, dataObj:data as! [String: AnyObject], delegate: delegate)
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCustomerRating, error: error, delegate:delegate)
        }
    }
    
    class func rejectJobRequestApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "jobs/rejectJob", params: params, showHUD: showHUD, success: { (status, data) in
            
//            if let data = data as? String {
//                self.sendResponseOfApi(api: ApiName.eApiRejectJobRequest, dataObj:data, delegate: delegate)
//            }else
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiRejectJobRequest, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiRejectJobRequest, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiRejectJobRequest, error: error, delegate:delegate)
        }
    }
    
    class func getChatApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "chat/get", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiGetChat, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiJobEditRequest, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiGetChat, error: error, delegate:delegate)
        }
    }
    
    class func sendChatApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "chat/send", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiSendChat, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiJobEditRequest, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiSendChat, error: error, delegate:delegate)
        }
    }
    
    class func creatIssueApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "insertIssue", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiCreatIssue, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiCreatIssue, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiCreatIssue, error: error, delegate:delegate)
        }
    }
    
    
    class func addBankInfoApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/bankinfo", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiAddBankInfo, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiAddBankInfo, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiAddBankInfo, error: error, delegate:delegate)
        }
    }
    
    class func getBankInfoApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.getRequest(url: "provider/bankinfo", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiGetBankInfo, dataObj:data, delegate: delegate)
                dispatchGroup.leave()
            }else{
                self.sendResponseOfApi(api: ApiName.eApiGetBankInfo, dataObj:data as! NSArray, delegate: delegate)
                dispatchGroup.leave()
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiGetBankInfo, error: error, delegate:delegate)
            dispatchGroup.leave()
        }
    }
    
    class func getForgetCodeApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/getforgetcode", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiGetFpCode, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiGetFpCode, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiGetFpCode, error: error, delegate:delegate)
        }
    }
    
    class func forgetPasswordApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/forgetpassword", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiForgetPassword, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiForgetPassword, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiForgetPassword, error: error, delegate:delegate)
        }
    }
    
    class func updatePasswordApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/updatePassword", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiUpdatePassword, dataObj:data, delegate: delegate)
            }else{
                self.sendResponseOfApi(api: ApiName.eApiUpdatePassword, dataObj:data as! NSArray, delegate: delegate)
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiUpdatePassword, error: error, delegate:delegate)
        }
    }
    
    class func weeklyEarningApi(params: [String:Any], delegate:NetworkManagerDelegate, showHUD: Bool){
        self.postRequestWithURL(url: "provider/weeklyearning", params: params, showHUD: showHUD, success: { (status, data) in
            
            if let data = data as? [String: AnyObject] {
                self.sendResponseOfApi(api: ApiName.eApiWeeklyEarning, dataObj:data, delegate: delegate)
                dispatchGroup.leave()
            }else{
                self.sendResponseOfApi(api: ApiName.eApiWeeklyEarning, dataObj:data as! NSArray, delegate: delegate)
                dispatchGroup.leave()
            }
            
        }) { (error) in
            self.sendResponseOfApiWithError(api: ApiName.eApiWeeklyEarning, error: error, delegate:delegate)
            dispatchGroup.leave()
        }
    }

}
