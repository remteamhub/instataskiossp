//
//  AppDelegate.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/9/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces
import IQKeyboardManagerSwift
import Stripe
import FBSDKCoreKit
import TwitterKit
import SwiftLocation
import iOS_Slide_Menu
import Firebase
import UserNotifications
import FirebaseMessaging
import SwiftLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var deviceTokenString:String = ""

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        Users.shared.loadUser()
        // Override point for customization after application launch.
        LocationManager.shared.showsBackgroundLocationIndicator = false
        LocationManager.shared.backgroundLocationUpdates = false
        GMSServices.provideAPIKey(MAPS_KEY)
        GMSPlacesClient.provideAPIKey(PLACES_API_KEY)
        
        FirebaseApp.configure()
        Messaging.messaging().delegate = self
        registerNotification()
        
        if let val = UserDefaults.standard.value(forKey: "FirstLogin") as? Bool {
            Extra.shared.firstTimeLogin = val
        }
        
        IQKeyboardManager.shared.enable = true
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        //TWITTER INIT
        TWTRTwitter.sharedInstance().start(withConsumerKey: TWITTER_CONSUMER_KEY, consumerSecret: TWITTER_CONSUMER_SECRET)
        //FACEBOOK INIT
        FBSDKApplicationDelegate.sharedInstance().application(application, didFinishLaunchingWithOptions: launchOptions)
        //LocationManager.shared.requireUserAuthorization(.whenInUse)
        
        //shared.requestAuthorizationIfNeeded(.whenInUse)
//        if let _ = launchOptions?[UIApplication.LaunchOptionsKey.location] {
//            LocationManager.shared.locateFromGPS(.significant, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
//                switch result {
//                case .success(let location):
//                    print("Location received AppDelegate: \(location.coordinate.latitude), \(location.coordinate.longitude)")
//                    Extra.shared.tempLatitude = location.coordinate.latitude
//                    Extra.shared.tempLongitude = location.coordinate.longitude
//                case .failure(let error):
//                    print("Received error: \(error)")
//                }
//            }
//        }
        
        
        if let val = UserDefaults.standard.value(forKey: "approval") as? Int {
            Extra.shared.jobApproval = val
        }
        application.registerForRemoteNotifications()
        
        SlideNavigationController.sharedInstance().enableShadow = true
        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
        SlideNavigationController.sharedInstance().leftMenu = secondViewController
        
        return true
    }
    
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
        if TWTRTwitter.sharedInstance().application(app, open: url, options: options) {
            return true
        }
        
        let handled = FBSDKApplicationDelegate.sharedInstance().application(app, open: url, options: options)
        return handled
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
        
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        
        UserDefaults.standard.synchronize()
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        NotificationCenter.default.post(.init(name: .refreshChat))
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
//        LocationManager.shared.startUpdatingLocation()
//        let position = CLLocationCoordinate2D(latitude:LocationManager.shared.location.coordinate.latitude, longitude: LocationManager.shared.location.coordinate.longitude)
//        LocationManager.shared.updateMarker(camPosition: position, map: LocationManager.shared.mapView, markerImage: "locationMarker")
        checkNotification()
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
        UserDefaults.standard.set(Extra.shared.jobApproval, forKey: "approval")
        UserDefaults.standard.synchronize()
    }

    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        print("\n didReceiveRemoteNotification \n", userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
        self.checkNotification(data: userInfo)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable: Any],
                     fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        print("\n didReceiveRemoteNotification with Completion Handler: \n\n", userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
//        if BackgroundTask.shared.application.applicationState != .background{
//            print("Background Blocccck")
//            self.checkNotification(data: userInfo)
//        }
        self.checkNotification(data: userInfo)
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        Messaging.messaging().apnsToken = deviceToken
        print("Here is Token: \(deviceTokenString)")
    }
    
    func registerNotification() {
        
        if #available(iOS 10.0, *) {
            let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
            UNUserNotificationCenter.current().requestAuthorization(
                options: authOptions,
                completionHandler: {_, _ in })
            
            UNUserNotificationCenter.current().delegate = self
            
        } else {
            let type: UIUserNotificationType = [UIUserNotificationType.badge, UIUserNotificationType.alert, UIUserNotificationType.sound]
            let setting = UIUserNotificationSettings(types: type, categories: nil)
            UIApplication.shared.registerUserNotificationSettings(setting)
            UIApplication.shared.registerForRemoteNotifications()
        }
        
    }
    
    func checkNotification(data: [AnyHashable: Any] = [:]) {
//        print("Message ID: \(data["gcm.message_id"])")
//        print("Body: \(data["body"])")
//        print("APS: \(data["aps"])")
        if let jobId = data["body"] as? NSString {
            if jobId == "approved" {
                Extra.shared.tempStatus = "0"
                UserDefaults.standard.set(0, forKey: approvalStatus)
                NotificationCenter.default.post(.init(name: .userStatusNotification))
                print("license Un approved")
            }
            if jobId == "unapproved" {
                Extra.shared.tempStatus = "1"
                UserDefaults.standard.set(1, forKey: approvalStatus)
                UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
                NotificationCenter.default.post(.init(name: .userStatusNotification))
                print("license approved")
            }
            if jobId == "jobDismiss" {
                NotificationCenter.default.post(.init(name: .dismissJobNotification))
            }
            if jobId == "You have new messages" {
                if Extra.shared.alreadyOnChat == false {
                    NotificationCenter.default.post(.init(name: .openSpNewMessage))
                }
            }
            //NetworkManager.limitationsApi(delegate: HomeViewController(), showHUD: false)
            
            //Extra.shared.checkJobById = Int((jobId).floatValue)
        }
        
        if let api = data["api"] as? String {
            print("Job ID : ", api)
            if api.contains("checkJobById_") {
                let data = api.components(separatedBy: "_")
                Extra.shared.checkJobById = Int((data[1]))!
                if data[0] == "checkJobById" {
                    Extra.shared.checkJobNotification = data[0]
                    NotificationCenter.default.post(.init(name: .jobByIdNotification))
                }
            }
            
             if api == "checkCurrentJob" {
                Extra.shared.checkNotification = api
                NotificationCenter.default.post(.init(name: .openRemoteNotification))
            }else if api == "editRequestStatus" {
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    NotificationCenter.default.post(.init(name: .openEditJobNotification))
                }
            }
            
            let res = api.replacingOccurrences(of: "/", with: " ").components(separatedBy: " ")
            if res.first! == "rating"{
                Extra.shared.tempCustomerJobID = res.last!
                NotificationCenter.default.post(.init(name: .openRatingUI))
            }
            return
        }else{
//                Extra.shared.checkNotification = "checkCurrentJob"
//                NotificationCenter.default.post(.init(name: .openRemoteNotification))
        }
    }
    
}


extension AppDelegate: UNUserNotificationCenterDelegate {
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse, withCompletionHandler completionHandler: @escaping () -> Swift.Void) {
        //Works when user tap on notification.
        print("UNUserNotification Did Recieve: ", response.notification.request.content.userInfo)
        
        let data = response.notification.request.content.userInfo
        Extra.shared.userClickedNotification = true
        Messaging.messaging().appDidReceiveMessage(data)

        self.checkNotification(data: data)
        completionHandler()
    }
    
    @available(iOS 10.0, *)
    func userNotificationCenter(_ center: UNUserNotificationCenter,
                                willPresent notification: UNNotification,
                                withCompletionHandler completionHandler: @escaping (UNNotificationPresentationOptions) -> Void) {
//        var content = UNMutableNotificationContent()
//        content.body = ""
//        content.title = notification.request.content.title
//        let req = UNNotificationRequest(identifier: "content", content: content, trigger: notification.request.trigger)
//        let userInfo = notification.request.content.userInfo
//        center.add(req) { (error) in
//            print(error?.localizedDescription)
//        }
        let userInfo = notification.request.content.userInfo
        Extra.shared.userClickedNotification = false
        //When user app is on.
        //print(userInfo )
        self.checkNotification(data: userInfo)
        Messaging.messaging().appDidReceiveMessage(userInfo)
    }
}

extension AppDelegate: MessagingDelegate {
    func messaging(_ messaging: Messaging, didReceiveRegistrationToken fcmToken: String) {
        let token = Messaging.messaging().fcmToken
        print("FCM token: \(token ?? "")")
        print("FCM token Two: \(fcmToken) ")
        deviceTokenString = fcmToken
    }
    func applicationReceivedRemoteMessage(_ remoteMessage: MessagingRemoteMessage) {
        print("%@", remoteMessage.appData)
    }
    public func messaging(_ messaging: Messaging, didReceive remoteMessage: MessagingRemoteMessage)
    {
        print("--->messaging:\(messaging)")
        print("--->didReceive Remote Message:\(remoteMessage.appData)")
        guard let data =
            try? JSONSerialization.data(withJSONObject: remoteMessage.appData, options: .prettyPrinted),
            let prettyPrinted = String(data: data, encoding: .utf8) else { return }
        print("Received direct channel message:\n\(prettyPrinted)")
    }
}
