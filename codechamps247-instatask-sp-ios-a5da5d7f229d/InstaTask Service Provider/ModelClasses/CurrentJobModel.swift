//
//  CurrentJobModel.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 5/13/19.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import Foundation
import ObjectMapper


class CurrentJobModel: Mappable {

    static let shared = CurrentJobModel()

    var id : Int = 0
    var location_address : String = ""
    var service_name : String = ""
    var service_id : Int = 0
    var provider_id : Int = 0
    var customer_approval : Int = 0
    var after_work_img : String = ""
    var job_status : Int = 0
    var lat : Double = 0.0
    var current_situation_img : String = ""
    var customer_id : Int = 0
    var lng  : Double = 0.0
    var user = UserModel()
    var service_price : Int = 0
    var provider = ProviderModel()
    var completeData = [Any]()
    var todayJobCount = Int()
    var todayJobEarnings = Int()
    var providerDetails = ProviderDetailsModel()
    var lastJob = LastJobModel()
    var currentDispatchedJob : CurrentDispatchedJob?
    var providerTimeLogs = ProviderTimeLogs()

    init() {

    }

    required init?(map: Map) {
        self.mapping(map: map)
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }

    func mapping(map: Map) {

        id                          <- map["id"]
        location_address            <- map["location_address"]
        service_name                <- map["service_name"]
        service_id                  <- map["service_id"]
        provider_id                 <- map["provider_id"]
        customer_approval           <- map["customer_approval"]
        after_work_img              <- map["after_work_img"]
        job_status                  <- map["job_status"]
        lat                         <- map["lat"]
        current_situation_img       <- map["current_situation_img"]
        customer_id                 <- map["customer_id"]
        lng                         <- map["lng"]
        user                        <- map["user"]
        service_price               <- map["service_price"]
        provider                    <- map["provider"]
        providerDetails             <- map["providerDetails"]
        lastJob                     <- map["lastJob"]
        currentDispatchedJob        <- map["currentDispatchedJob"]
        providerTimeLogs            <- map["providerTimeLogs"]
        todayJobCount               <- map["todayJobCount"]
        todayJobEarnings            <- map["todayJobEarnings"]
    }

}

class UserModel: Mappable {

    static let shared = UserModel()

    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0

    init() {

    }

    required init?(map: Map) {
        self.mapping(map: map)
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }

    func mapping(map: Map) {

        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]

    }

}


class ProviderModel: Mappable {

    static let shared = ProviderModel()

    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0

    init() {

    }

    required init?(map: Map) {
        self.mapping(map: map)
    }

    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }

    func mapping(map: Map) {

        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]

    }

}



//
//class CheckCurrentJobModel {
//
//    static let shared = CheckCurrentJobModel()
//
//    var providerDetails = ProviderDetailsModel()
//    var job : JobModel?
//    var lastJob = LastJobModel()
//    var currentDispatchedJob = CurrentDispatchedJob()
//    var providerTimeLogs = ProviderTimeLogs()
//    var todayJobCount = Int()
//    var todayJobEarnings = Int()
//
//    init() {
//
//    }
//
//    required init?(map: Map) {
//        self.mapping(map: map)
//    }
//
//    convenience init(dic:[String:Any]) {
//        let map = Map.init(mappingType: .fromJSON, JSON: dic)
//        self.init(map:map)!
//    }
//
//    func mapping(map: Map) {
//
//        providerDetails                     <- map["providerDetails"]
//        job                                 <- map["job"]
//        lastJob                             <- map["lastJob"]
//        currentDispatchedJob                <- map["currentDispatchedJob"]
//        providerTimeLogs                    <- map["providerTimeLogs"]
//        todayJobCount                       <- map["todayJobCount"]
//        todayJobEarnings                    <- map["todayJobEarnings"]
//
//    }
//
//}




class ProviderDetailsModel: Mappable {
    
    static let shared = ProviderDetailsModel()
    
    var id : Int = 0
    var user_id : Int = 0
    var license_img : String = ""
    var radius : Int = 0
    var approval_status : Int = 0
    var job_status : Int = 0
    var withdraw_status : Int = 0
    var total_earning : Int = 0
    var weekly_earning : Int = 0
    var overall_rating : Int = 0
    var created_at : String = ""
    var updated_at : String = ""
    var queued_job : Int = 0
    var job_count : Int = 0
   
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        user_id                         <- map["user_id"]
        license_img                     <- map["license_img"]
        radius                          <- map["radius"]
        approval_status                 <- map["approval_status"]
        job_status                      <- map["job_status"]
        withdraw_status                 <- map["withdraw_status"]
        total_earning                   <- map["total_earning"]
        weekly_earning                  <- map["weekly_earning"]
        overall_rating                  <- map["overall_rating"]
        updated_at                      <- map["updated_at"]
        created_at                      <- map["created_at"]
        queued_job                      <- map["queued_job"]
        job_count                       <- map["job_count"]
        
    }
    
}

class JobModel: Mappable {
    
    static let shared = JobModel()
   
    var id : Int = 0
    var service_id : Int = 0
    var service_name : String = ""
    var service_price : Int = 0
    var provider_id : Int = 0
    var customer_id : Int = 0
    var current_situation_img : String = ""
    var after_work_img : String = ""
    var customer_approval : Int = 0
    var job_status : Int = 0
    var lat : Double = 0
    var lng : Double = 0
    var location_address : String = ""
    var job_schedual_time : Int = 0
    var auto_dispatch : Int = 0
    var customer_rating : Int = 0
    var driver_rating : Int = 0
    var feedback : String = ""
    var edit_job : Int = 0
    var admin_notify : Int = 0
    var user = JobUserModel()
    var provider = JobProviderModel()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                           <- map["id"]
        service_id                   <- map["service_id"]
        service_name                 <- map["service_name"]
        service_price                <- map["service_price"]
        provider_id                  <- map["provider_id"]
        job_status                   <- map["job_status"]
        customer_id                  <- map["customer_id"]
        current_situation_img        <- map["current_situation_img"]
        after_work_img               <- map["after_work_img"]
        customer_approval            <- map["customer_approval"]
        lat                          <- map["lat"]
        lng                          <- map["lng"]
        location_address             <- map["location_address"]
        job_schedual_time            <- map["job_schedual_time"]
        auto_dispatch                <- map["auto_dispatch"]
        customer_rating              <- map["customer_rating"]
        driver_rating                <- map["driver_rating"]
        feedback                     <- map["feedback"]
        edit_job                     <- map["edit_job"]
        admin_notify                 <- map["admin_notify"]
        user                         <- map["user"]
        provider                     <- map["provider"]
        
    }
    
}

class JobUserModel: Mappable {
    
    static let shared = JobUserModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class JobProviderModel: Mappable {
    
    static let shared = JobProviderModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class LastJobModel: Mappable {
    
    static let shared = LastJobModel()
    
    var id : Int = 0
    var service_id : Int = 0
    var service_name : String = ""
    var service_price : Int = 0
    var provider_id : Int = 0
    var customer_id : Int = 0
    var current_situation_img : String = ""
    var after_work_img : String = ""
    var customer_approval : Int = 0
    var job_status : Int = 0
    var lat : Double = 0
    var lng : Double = 0
    var location_address : String = ""
    var job_schedual_time : Int = 0
    var auto_dispatch : Int = 0
    var customer_rating : Int = 0
    var driver_rating : Int = 0
    var feedback : String = ""
    var edit_job : Int = 0
    var admin_notify : Int = 0
    var user = LastUserModel()
    var provider = LastProviderModel()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                           <- map["id"]
        service_id                   <- map["service_id"]
        service_name                 <- map["service_name"]
        service_price                <- map["service_price"]
        provider_id                  <- map["provider_id"]
        job_status                   <- map["job_status"]
        customer_id                  <- map["customer_id"]
        current_situation_img        <- map["current_situation_img"]
        after_work_img               <- map["after_work_img"]
        customer_approval            <- map["customer_approval"]
        lat                          <- map["lat"]
        lng                          <- map["lng"]
        location_address             <- map["location_address"]
        job_schedual_time            <- map["job_schedual_time"]
        auto_dispatch                <- map["auto_dispatch"]
        customer_rating              <- map["customer_rating"]
        driver_rating                <- map["driver_rating"]
        feedback                     <- map["feedback"]
        edit_job                     <- map["edit_job"]
        admin_notify                 <- map["admin_notify"]
        user                         <- map["user"]
        provider                     <- map["provider"]
        
    }
    
}

class LastUserModel: Mappable {
    
    static let shared = LastUserModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class LastProviderModel: Mappable {
    
    static let shared = LastProviderModel()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class ProviderTimeLogs: Mappable {
    
    static let shared = ProviderTimeLogs()
    
    var important = String()
    var todayWorkingHours = Int()
    var lastLoginTime = Int()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        important                              <- map["important"]
        todayWorkingHours                      <- map["todayWorkingHours"]
        lastLoginTime                          <- map["lastLoginTime"]
       
    }
    
}




class CurrentDispatchedJob: Mappable {
    
    var id = Int()
    var service_id = Int()
    var service_name = String()
    var service_price = Int()
    var provider_id = Int()
    var customer_id = Int()
    var current_situation_img = String()
    var after_work_img = String()
    var customer_approval = Int()
    var job_status = Int()
    var lat = Double()
    var lng = Double()
    var location_address = String()
    var job_schedual_time = Int()
    var auto_dispatch = Int()
    var customer_rating = Int()
    var driver_rating = Int()
    var feedback = String()
    var edit_job = Int()
    var admin_notify = Int()
    var user = CurrentDispatchedJobUser()
    var service = CurrentDispatchedJobService()
    
    static let shared = CurrentDispatchedJob()

    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                             <- map["id"]
        service_id                     <- map["service_id"]
        service_name                   <- map["service_name"]
        service_price                  <- map["service_price"]
        provider_id                    <- map["provider_id"]
        customer_id                    <- map["customer_id"]
        current_situation_img          <- map["current_situation_img"]
        after_work_img                 <- map["after_work_img"]
        customer_approval              <- map["customer_approval"]
        job_status                     <- map["job_status"]
        lat                            <- map["lat"]
        lng                            <- map["lng"]
        location_address               <- map["location_address"]
        job_schedual_time              <- map["job_schedual_time"]
        auto_dispatch                  <- map["auto_dispatch"]
        customer_rating                <- map["customer_rating"]
        driver_rating                  <- map["driver_rating"]
        edit_job                       <- map["edit_job"]
        admin_notify                   <- map["admin_notify"]
        feedback                       <- map["feedback"]
        user                           <- map["user"]
        service                        <- map["service"]
        
    }
    
    
}




class CurrentDispatchedJobUser: Mappable {
    
    static let shared = CurrentDispatchedJobUser()
    
    var id : Int = 0
    var first_name : String = ""
    var last_name : String = ""
    var phone : String = ""
    var created_at : String = ""
    var type : Int = 0
    var email_verified_at : String = ""
    var avatar : String = ""
    var address : String = ""
    var social_id : String = ""
    var updated_at : String = ""
    var username : String = ""
    var fcm_token : String = ""
    var socket_token : String = ""
    var lat : Double = 0.0
    var email : String = ""
    var lng : Double = 0.0
    var status : Int = 0
    var rating : Float = 0.0
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                              <- map["id"]
        first_name                      <- map["first_name"]
        last_name                       <- map["last_name"]
        phone                           <- map["phone"]
        created_at                      <- map["created_at"]
        type                            <- map["type"]
        email_verified_at               <- map["email_verified_at"]
        avatar                          <- map["avatar"]
        address                         <- map["address"]
        social_id                       <- map["social_id"]
        updated_at                      <- map["updated_at"]
        username                        <- map["username"]
        fcm_token                       <- map["fcm_token"]
        socket_token                    <- map["socket_token"]
        lat                             <- map["lat"]
        email                           <- map["email"]
        lng                             <- map["lng"]
        status                          <- map["status"]
        rating                          <- map["rating"]
        
    }
    
}


class CurrentDispatchedJobService: Mappable {
    
    static let shared = CurrentDispatchedJobService()
    
    var id = Int()
    var title = String()
    var status = Int()
    var price = Int()
    var commission = Int()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                         <- map["id"]
        title                      <- map["title"]
        status                     <- map["status"]
        price                      <- map ["price"]
        commission                 <- map ["commision"]
        
    }
    
}
