//
//  LeftMenuItem.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation

class LeftMenuItem: NSObject {
    
    var imageName: String?
    var cellLabel: String?
    var method: String?
    
    init(imageName: String, cellLabel : String , method : String) {
        
        self.imageName = imageName
        self.cellLabel = cellLabel
        self.method = method
        
    }
    
}
