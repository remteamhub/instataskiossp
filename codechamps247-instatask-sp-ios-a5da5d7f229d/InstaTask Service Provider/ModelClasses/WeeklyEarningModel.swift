//
//  WeeklyEarningModel.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 25/10/2019.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import Foundation
import ObjectMapper

class WeeklyEarningModel: Mappable {
    
    static let shared = WeeklyEarningModel()
    
    var amount : Int = 0
    var job_count : Int = 0
    var dates = DatesModel()
    
    var weeklyEarningArray = [Any]()
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        amount                   <- map["amount"]
        job_count              <- map["job_count"]
        dates            <- map["dates"]
        
    }
    
}

class DatesModel: Mappable {
    
    static let shared = DatesModel()
    
    var end : String = ""
    var start : String = ""
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        end                   <- map["end"]
        start              <- map["start"]
        
    }
    
}
