//
//  WithdrawModel.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 23/10/2019.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import Foundation
import ObjectMapper

class WithdrawModel: Mappable {
    
    static let shared = WithdrawModel()
    
    var id : Int = 0
    var country : String = ""
    var bank_name : String = ""
    var swift_code : String = ""
    var account_no : String = ""
    var IBAN : String = ""
    
    
    init() {
        
    }
    
    required init?(map: Map) {
        self.mapping(map: map)
    }
    
    convenience init(dic:[String:Any]) {
        let map = Map.init(mappingType: .fromJSON, JSON: dic)
        self.init(map:map)!
    }
    
    func mapping(map: Map) {
        
        id                   <- map["id"]
        country              <- map["country"]
        bank_name            <- map["bank_name"]
        swift_code           <- map["swift_code"]
        account_no           <- map["account_no"]
        IBAN                 <- map["IBAN"]

    }
    
}
