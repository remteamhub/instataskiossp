//
//  Utilities.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/20/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation
import Alamofire
import Photos
import PKHUD
import UIKit

protocol RedirectDelegate: class {
    func redirectTo()
}

class Utilities: NSObject {
    
//    static var hud = MBProgressHUD()
    
    weak static var delegate: RedirectDelegate?
    
    //MARK:- Reachability checks
    
    class func isNetworkReachable() -> Bool {
        let networkReachabilityManager = Alamofire.NetworkReachabilityManager(host: "www.google.com")
        return (networkReachabilityManager?.isReachable)!
    }
    
    class func getDocumentsDirectory() -> URL {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask)
        return paths[0]
    }//MARK: - Progress loader code block
    
//    class func showProgressLoader(view : UIView) {
//        DispatchQueue.main.async {
//            hud = MBProgressHUD.showAdded(to: view, animated: true)
//            hud.show(animated: true)
//        }
//    }
//
//    class func hideProgressLoader(view : UIView) {
//        DispatchQueue.main.async{
//            hud.hide(animated: true)
//            MBProgressHUD.hide(for: view, animated: true)
//        }
//    }
    
    
    class func UnixToCurrentTime(unixTime: Int) -> String {
        print(unixTime)
        print(TimeInterval(unixTime))
        let date = Date(timeIntervalSince1970: TimeInterval(unixTime))
        print(date)
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    class func currentToUnix() -> String{
//
////        let currentDateTime = Date()
////
////        // initialize the date formatter and set the style
////        let formatter = DateFormatter()
////        formatter.timeStyle = .medium
////        formatter.dateStyle = .long
////
////        print(formatter.string(from: currentDateTime))
////
////        // get the date time String from the date object
////         return localToUTC(date: formatter.string(from: currentDateTime))
//
//        // get the current date and time
//        let currentDateTime = Date()
//
//        // initialize the date formatter and set the style
//        let formatter = DateFormatter()
//
//        // "October 8, 2016 at 10:52:30 PM GMT+8"
//        formatter.timeStyle = .long
//        formatter.dateStyle = .long
//        formatter.string(from: currentDateTime)
//        formatter.calendar = NSCalendar.current
//        //formatter.timeZone = TimeZone.current
//        formatter.timeZone = TimeZone(abbreviation: "UTC")

        //print(formatter.string(from: currentDateTime))
        let date = NSDate() // current date
        let unixtime = date.timeIntervalSince1970
        return unixtime.description.components(separatedBy: ".").first!
    }
    
    class func localToUTC(date:String) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "h:mm a"
        dateFormatter.calendar = NSCalendar.current
        dateFormatter.timeZone = TimeZone.current
        
        let dt = dateFormatter.date(from: date)
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        dateFormatter.dateFormat = "H:mm:ss"
        
        return dateFormatter.string(from: dt!)
    }

    
    class func UnixToTime(unixTime: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(unixTime))
        let dateFormatter = DateFormatter()
        dateFormatter.timeStyle = DateFormatter.Style.medium //Set time style
//        dateFormatter.dateStyle = DateFormatter.Style.medium //Set date style
        dateFormatter.timeZone = TimeZone.current
        let localDate = dateFormatter.string(from: date)
        return localDate
    }
    
    func getTodayString() -> String{
        
        let date = Date()
        let calender = Calendar.current
        let components = calender.dateComponents([.hour,.minute,.second], from: date)
        
        let year = components.year
        let month = components.month
        let day = components.day
        let hour = components.hour
        let minute = components.minute
        let second = components.second
        
        let today_string = String(year!) + "-" + String(month!) + "-" + String(day!) + " " + String(hour!)  + ":" + String(minute!) + ":" +  String(second!)
        
        return today_string
        
    }

    
    
    class func showHUD() {
        
        let view = PKHUDRotatingImageView.init(image: UIImage(named:"loading"), title: "", subtitle: "")
        view.frame = CGRect(x: 0, y: 0, width: 130, height: 130)
        view.backgroundColor = .clear
        PKHUD.sharedHUD.contentView = view
        PKHUD.sharedHUD.dimsBackground = true
        
        PKHUD.sharedHUD.show()
    }
    
    class func hideHUD() {
        PKHUD.sharedHUD.hide()
    }
    
    class func printPrettyJSONFromDic(params:[String:Any]) {
        let jsonData = try! JSONSerialization.data(withJSONObject: params, options: JSONSerialization.WritingOptions.prettyPrinted)
        let jsonString = NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
        print("Response: \(jsonString)")
    }
    
    class func generateError(message:String) -> NSError {
        let userInfo: [String : Any] =
            [
                NSLocalizedDescriptionKey :  NSLocalizedString("er", value: message, comment: "")
        ]
        return NSError(domain: "ShiploopHttpResponseErrorDomain", code: 401, userInfo: userInfo)
    }
    
//    class func isKeyPresentInUserDefaults(key: String) -> Bool {
//        return UserDefaults.standard.object(forKey: key) != nil
//    }
//    
//    //MARK:- UserDefaults Saving
//    class func saveToken(token: String)
//    {
//        UserDefaults.standard.set(token, forKey: "authToken")
//        UserDefaults.standard.synchronize()
//    }
//    
//    class func getToken() -> String
//    {
//        return UserDefaults.standard.string(forKey: "authToken")!
//    }
    
    
    class func showAlert(title:String, message:String, delegate: RedirectDelegate? = nil) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        let okAction = UIAlertAction(title: "Ok", style: UIAlertAction.Style.default) { (action) in
            if delegate != nil {
                delegate?.redirectTo()
            }
        }
        alert.addAction(okAction)
        let win = APP_Delegate.window
        win?.rootViewController?.present(alert, animated: true, completion: nil)
    }
    
//    class func saveCurrentUser(user: Users)
//    {
//        let data = NSKeyedArchiver.archivedData(withRootObject: user)
//        UserDefaults.standard.set(data, forKey: "currentUser")
//        UserDefaults.standard.synchronize()
//    }
//    
//    class func getCurrentUser() -> Users
//    {
//        var user = Users()
//        let usersData = UserDefaults.standard.object(forKey: "currentUser") as? NSData
//        if let loggedInUsersData = usersData {
//            if let  userData = NSKeyedUnarchiver.unarchiveObject(with: loggedInUsersData as Data) as? Users {
//                user = userData
//            }
//        }
//        return user
//    }
//    
//    class func removeCurrentUser()
//    {
//        UserDefaults.standard.removeObject(forKey: "currentUser")
//        UserDefaults.standard.synchronize()
//    }
    
    //MARK:- VALIDATORS
    class func isValidEmail(testStr:String) -> Bool {
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,64}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
}

extension Dictionary {
    
    static func += (left: inout [Key: Value], right: [Key: Value]) {
        for (key, value) in right {
            left[key] = value
        }
    }
}
