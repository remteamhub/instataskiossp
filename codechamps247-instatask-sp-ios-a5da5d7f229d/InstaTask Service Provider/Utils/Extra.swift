//
//  Extra.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 4/27/19.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import Foundation
import GoogleMaps
import GooglePlaces

class Extra: NSObject {
    static let shared = Extra()
    
    var tempParams = [String:Any]()
    var tempLatitude = Double()
    var tempLongitude = Double()
    var tempRadius:Int = 3
    var tempLoginCheck = false
    var tempCustomerName = String()
    var checkNotification = String()
    var checkJobNotification = String()
    var checkJobById  = Int()
    var tempJobId = Int()
    var tempStatus = String()
    var socialLoginCheck = false
    var jobApproval = -1
    var tempJobStatus = 3
    var seconds = Int()
    var maxJobAcceptance = Int()
    var tempCustomerJobID = String()
    var alreadyOnChat = false
    var userClickedNotification = false
    var firstTimeLogin = false
    var myMarkersDictionary = [String:GMSMarker]()
    let marker = GMSMarker()
    var vi_Map:GMSMapView?
    
    func checkJobStatus(status: WorkStatus) -> Int {
        switch status {
        case .free:
            return 0
        case .busy:
            return 1
        case .waiting:
            return 2
        case .signedout:
            return 3
        }
    }
    
    func checkStatus(status: JobStatus) -> Int {
        switch status {
        case .complete:
            return 0
        case .leave_for_job:
            return 1
        case .pending:
            return 2
        case .cancel:
            return 3
        case .accept:
            return 4
        case .working:
            return 5
        case .timeout:
            return 6
        case .confirmArrival:
            return 7
        case .requestApproval:
            return 8
        }
    }
    
    func checkApprovalStatus(status: AdminApprovalStatus) -> Int {
        switch status {
        case .approve:
            return 0
        case .unApprove:
            return 1
        }
    }
    
    func checkCurrentState(view: CurrentState) -> CurrentState {
        switch view {
        case .lastJobView:
            return .lastJobView // default view
        case .jobRequestView:
            return .jobRequestView
        case .leaveforJobView:
            return .leaveforJobView
        case .jobStartedView:
            return .jobStartedView
        case .confirmArrivalView:
            return .confirmArrivalView
        }
    }
    
    func userDefaultDataSave(user: Users, providerDetails: [String:Any]){
        UserDefaults.standard.set(user.job_status, forKey: ONLINE_STATUS)
        UserDefaults.standard.set(providerDetails["approval_status"], forKey: approvalStatus)
    }
    
    func UserDefaultDataRemove(){
        UserDefaults.standard.removeObject(forKey: ONLINE_STATUS)
        UserDefaults.standard.removeObject(forKey: approvalStatus)
        UserDefaults.standard.removeObject(forKey: My_Pass)
        UserDefaults.standard.removeObject(forKey: findDriver)
    }
}

enum JobStatus:Int {
    case complete = 0
    case leave_for_job = 1
    case pending = 2
    case cancel = 3
    case accept = 4
    case working = 5
    case timeout = 6
    case confirmArrival = 7
    case requestApproval = 8
}

enum WorkStatus:Int {
    case free = 0
    case busy = 1
    case waiting = 2
    case signedout = 3
}

enum UserStatus:Int {
    case active = 0
    case inactive = 1
    case banned = 2
    case pending = 3
}

enum AdminApprovalStatus:Int {
    case approve = 0
    case unApprove = 1
}

enum CurrentState: Int {
    case lastJobView = 0 // default view
    case jobRequestView = 1
    case leaveforJobView = 2
    case jobStartedView = 3
    case confirmArrivalView = 4
}
