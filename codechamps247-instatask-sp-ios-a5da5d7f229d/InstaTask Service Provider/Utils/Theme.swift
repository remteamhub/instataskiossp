//
//  Theme.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit

let mainBlue = UIColor.init(red: 1/255, green: 72/255, blue: 126/255, alpha: 1.0)

class Theme
{
    class func primaryFontWithSize(size: CGFloat? = 16) -> UIFont
    {
        let primaryFontName = "SanFranciscoDisplay-Regular"
        return UIFont.init(name: primaryFontName, size: size!)!
    }
    
    class func semiBoldFontWithSize(size: CGFloat? = 18) -> UIFont
    {
        let semiBoldName = "SanFranciscoDisplay-Semibold"
        return UIFont.init(name: semiBoldName, size: size!)!
    }
    
    //    class func boldFontWithSize(size: CGFloat? = 20) -> UIFont
    //    {
    //        let boldName = "Muli-Bold"
    //        return UIFont.init(name: boldName, size: size!)!
    //    }
}
