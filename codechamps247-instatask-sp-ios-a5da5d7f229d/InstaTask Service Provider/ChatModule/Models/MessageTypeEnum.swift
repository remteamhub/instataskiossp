//
//  MessageTypeEnum.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 11/27/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation

enum MessageTypeEnum: String {
    case image = "image"
    case url = "url"
    case text = "text"
    case system = "system"
}
