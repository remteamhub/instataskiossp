////
////  URLDisplayViewController.swift
////  SnikPic
////
////  Created by Asfand Shabbir on 12/3/18.
////  Copyright © 2018 Aqsa Arshad. All rights reserved.
////
//
//import UIKit
//
//class URLDisplayViewController: UIViewController {
//    
//    @IBOutlet weak var tableView: UITableView!
//    
//    var URLsArray: [TrackingURL] = []
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//        // Do any additional setup after loading the view.
//        setupUI()
//        
//        ChatUtilities.showProgressLoader(view: self.view)
//        DataStore.shared.readURLs { (urls) in
//            DispatchQueue.main.async {
//                self.URLsArray = urls
//                self.tableView.reloadData()
//                ChatUtilities.hideProgressLoader(view: self.view)
//            }
//        }
//    }
//    
//    func setupUI()
//    {
//        tableView.delegate = self
//        tableView.dataSource = self
//        tableView.tableFooterView = UIView()
//        tableView.backgroundColor = UIColor.init(rgb: 0xF7F7FB)
//        
//        title = "Order URLs"
//        
//        let backButton = UIButton.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
//        backButton.setTitle("Back", for: .normal)
//        backButton.setTitleColor(UIColor.black.withAlphaComponent(0.45), for: .normal)
//        backButton.titleLabel?.font = Theme.getDefaultFont().withSize(12.0)
//        backButton.titleLabel?.textAlignment = .center
//        backButton.setImage(UIImage.init(named: "backArrow")?.withRenderingMode(.alwaysTemplate), for: .normal)
//        backButton.imageView?.tintColor = UIColor.black
//        backButton.imageEdgeInsets = UIEdgeInsets.init(top: 15, left: 0, bottom: 15, right: 15)
//        backButton.addTarget(self, action: #selector(backButtonPressed), for: .touchUpInside)
//        backButton.imageView?.contentMode = .scaleAspectFit
//        backButton.contentHorizontalAlignment = .left
//        navigationItem.leftBarButtonItems = [UIBarButtonItem.init(customView: backButton)]
//        
//        //TITLE VIEW
//        let topTitleView = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 140, height: 40))
//        topTitleView.textAlignment = .center
//        topTitleView.text = "URLs"
//        topTitleView.font = Theme.getDefaultFont().withSize(16.0)
//        navigationItem.titleView = topTitleView
//    }
//    
//
//    /*
//    // MARK: - Navigation
//
//    // In a storyboard-based application, you will often want to do a little preparation before navigation
//    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
//        // Get the new view controller using segue.destination.
//        // Pass the selected object to the new view controller.
//    }
//    */
//
//    //MARK:- TOP BAR ACTIONS
//    @objc func backButtonPressed()
//    {
//        navigationController?.popViewController(animated: true)
//    }
//    
//}
//
//extension URLDisplayViewController: UITableViewDelegate, UITableViewDataSource
//{
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//    
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        return URLsArray.count
//    }
//    
//    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//        let view = UIView.init(frame: CGRect.init(x: 0, y: 0, width: tableView.frame.width, height: 1.0))
//        view.backgroundColor = UIColor.init(rgb: 0xF7F7FB)
//        return view
//    }
//    
//    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 1.0
//    }
//    
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "URLDisplayCell") as! URLDisplayCell
//        cell.urlLabel.text = URLsArray[indexPath.row].url
//        cell.urlLabel.textColor = .black
//        cell.selectionStyle = .none
//        return cell
//    }
//    
//    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//        if let url = URL(string: URLsArray[indexPath.row].url) {
//            UIApplication.shared.open(url, options: [:])
//        }
//    }
//    
//}
