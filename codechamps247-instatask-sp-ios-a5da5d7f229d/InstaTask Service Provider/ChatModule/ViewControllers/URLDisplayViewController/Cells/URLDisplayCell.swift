//
//  URLDisplayCell.swift
//  SnikPic
//
//  Created by Asfand Shabbir on 12/3/18.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import UIKit

class URLDisplayCell: UITableViewCell {

    @IBOutlet weak var urlLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
