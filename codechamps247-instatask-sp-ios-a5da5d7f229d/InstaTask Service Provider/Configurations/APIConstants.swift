//
//  APIConstants.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/10/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation

let baseURL = "http://trigoncab.com/instatask/api/"//"http://134.209.115.28/api/"
let imageBackURL = "http://trigoncab.com/instatask/storage/app/"//"http://134.209.115.28/storage/app/"

//"http://mce-foody.tk/admin/"//"http://dummy.restapiexample.com/api/v1/"//"http://54.89.144.217:6565/" //"https://instatask-node.herokuapp.com/"
let socketURL = "http://134.209.115.28:3000/"
let constantUserAuth = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1YzBkMjRmZGZmMWQ0NTAwMTVjMWIxMmIiLCJhY2Nlc3MiOiJhdXRoIiwiZXhwaXJ5IjoiMjAxOS0wMS0wOVQxODowNzozNSswMDowMCIsImlhdCI6MTU0NDQ2NTI1NX0.wXcnSuWxFqZaFJM2g19wakO6KK8ttcDzSlw_S2YrD3A"

let ephimeralKeyURL = baseURL + "ephemeral-key/service-provider"


//CUSTOMER API END POINTS
let registerURL = baseURL + "register/service-provider"
let loginURL = baseURL + "login/service-provider"
let licenseURL = baseURL + "license/service-provider"


let STRIPE_API_VERSION = "2018-11-08"

