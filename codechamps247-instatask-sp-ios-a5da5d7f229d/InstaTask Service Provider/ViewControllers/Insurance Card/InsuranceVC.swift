//
//  InsuranceVC.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 23/01/2020.
//  Copyright © 2020 Mustafa Shaheen. All rights reserved.
//

import UIKit
import Photos
import Kingfisher

class InsuranceVC: UIViewController {
    
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var successLabel: UILabel!
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var licenseImgView: UIImageView!
   
    var fileURL:URL?
    var licenseImage: URL?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()
    {
        submitButton.layer.cornerRadius = 8
        uploadButton.layer.cornerRadius = 6
        
        successLabel.alpha = 0.0
        
        self.uploadButton.isEnabled = true
        self.submitButton.isEnabled = false
        licenseImgView.contentMode = .scaleAspectFit
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        if(segue.identifier == "successLicenseSegue")
//        {
//            self.navigationController?.isNavigationBarHidden = false
//        }
        
    }
 
    
    @IBAction func uploadButtonPressed(_ sender: Any) {
        
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
        actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
            return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
            }.first?.isActive = false
    }
    
    @IBAction func onclick_Back(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func submitButtonPressed(_ sender: Any) {
        if Extra.shared.tempLoginCheck == true {
            Extra.shared.socialLoginCheck = true
            if fileURL != nil {
                NetworkManager.updateLicenseApi(params: ["user_id":"\(Users.shared.user_id)", "fcm_token":"\(APP_Delegate.deviceTokenString)"], file: fileURL!, insurance: licenseImage, image: "license_img", delegate: self, showHUD: true)
            }else{
                Utilities.showAlert(title: "Alert!", message: "Select image it is required.")
            }
        }else{
            if fileURL != nil {
                Extra.shared.socialLoginCheck = false
                NetworkManager.signUpApi(params: Extra.shared.tempParams, file: fileURL!, insurance: licenseImage, delegate: self, showHUD: true)
            }else{
                Utilities.showAlert(title: "Alert!", message: "Select image it is required.")
            }
        }

    }
    
}


extension InsuranceVC: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        
        if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[.editedImage] as? UIImage {
            selectedImage = editImage
        }
        
        picker.dismiss(animated: true, completion: nil)
        
        if let data = selectedImage!.jpegData(compressionQuality: 0.5){
            
            self.fileURL = Utilities.getDocumentsDirectory()
            self.fileURL!.appendPathComponent("insurance_image.png")
            
            
            if FileManager.default.fileExists(atPath: self.fileURL!.path){
                do{
                    try FileManager.default.removeItem(at: fileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: self.fileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            let provider = LocalFileImageDataProvider(fileURL: self.fileURL!)
            self.licenseImgView.kf.setImage(with: provider, options: [.forceRefresh])
            
            successLabel.alpha = 1.0
            self.submitButton.isEnabled = true

        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}

extension InsuranceVC: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiSignUp {
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            Extra.shared.firstTimeLogin = true
            UserDefaults.standard.set(true, forKey: "FirstLogin")
            Utilities.printPrettyJSONFromDic(params: providerDetails)
            let user = Users.init(dic: providerDetails)
            Users.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            Extra.shared.userDefaultDataSave(user: user, providerDetails: providerDetails)
            self.performSegue(withIdentifier: "successLicenseSegue", sender: self)
        }else if api == .eApiUpdateLicense {
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            Extra.shared.firstTimeLogin = true
            UserDefaults.standard.set(true, forKey: "FirstLogin")
            Utilities.printPrettyJSONFromDic(params: providerDetails)
            let user = Users.init(dic: providerDetails)
            Users.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            Extra.shared.userDefaultDataSave(user: user, providerDetails: providerDetails)
            self.performSegue(withIdentifier: "successLicenseSegue", sender: self)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiSignUp {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiUpdateLicense {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}
