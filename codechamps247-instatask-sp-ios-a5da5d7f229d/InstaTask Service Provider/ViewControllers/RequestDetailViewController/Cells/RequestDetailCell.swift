//
//  RequestDetailCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/29/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import GoogleMaps
import HCSStarRatingView

class RequestDetailCell: UITableViewCell {

    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var profileImgView: UIImageView!
    @IBOutlet weak var earningDetails: UILabel!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var rating: HCSStarRatingView!
    @IBOutlet weak var beforeStaringImg: UIImageView!
    @IBOutlet weak var afterStaringImg: UIImageView!
    @IBOutlet weak var customerImg: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width / 2
        profileImgView.clipsToBounds = true
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateMarker(lat: Double, long: Double)
    {
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
        mapView.camera = camera
        
        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "locationMarker")
        marker.map = mapView
        
        mapView.isUserInteractionEnabled = false
    }
    
    func setImages(current situationImage: String, after situation:String, customer avatar: String){
        self.beforeStaringImg.kf.indicatorType = .activity
        self.beforeStaringImg.kf.setImage(with: URL(string: situationImage), placeholder: UIImage.init(named: "home-noImageAdded"), options: [.transition(.fade(0.2))])
        self.afterStaringImg.kf.indicatorType = .activity
        self.afterStaringImg.kf.setImage(with: URL(string: situation), placeholder: UIImage.init(named: "home-noImageAdded"), options: [.transition(.fade(0.2))])
        self.customerImg.kf.indicatorType = .activity
        self.customerImg.kf.setImage(with: URL(string: avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
    }
    
}
