//
//  CustomerChatController.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 10/19/19.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import UIKit

class CustomerChatController: UIViewController {

    @IBOutlet weak var messageTF: UITextField!
    @IBOutlet weak var chatTV: UITableView!
    
       var jobModel = PendingJobModel()
        var check = false
        var paginateData = String()
        var refreshControl:UIRefreshControl!
        
    
        override func viewDidLoad() {
            super.viewDidLoad()
            self.whiteTopTitle(title: "Chat")
            
            print(jobModel.user.id)
            print(jobModel.customer_id)
            Extra.shared.alreadyOnChat = true
            Extra.shared.userClickedNotification = false
            
            refreshControl = UIRefreshControl()
            refreshControl.tintColor = mainBlue
            refreshControl.addTarget(self, action: #selector(refresh(sender:)), for: UIControl.Event.valueChanged)
            chatTV.addSubview(refreshControl)
            
            let params = [
                "user1":"\(jobModel.user.id)",
                "user2":"\(Users.shared.id)"
            ]
            NetworkManager.getChatApi(params: params, delegate: self, showHUD: false)
            
        }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        Extra.shared.alreadyOnChat = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        NotificationCenter.default.addObserver(self, selector: #selector(reloadChat), name: .refreshChat , object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.removeObserver(self, name: .refreshChat, object: nil)
    }
    
    
    @objc func reloadChat(sender:Any) {
           let params = [
               "user1":"\(jobModel.user.id)",
               "user2":"\(Users.shared.id)"
           ]
           NetworkManager.getChatApi(params: params, delegate: self, showHUD: false)
       }
    
        @objc func refresh(sender:Any) {
            print("refreshing...")
            if paginateData != "" {
                if let range = paginateData.range(of: "=") {
                    let lastCharacters = paginateData[range.upperBound...].trimmingCharacters(in: .whitespaces)
                    check = true
                    let params = [
                        "user1":"\(jobModel.provider.id)",
                        "user2":"\(Users.shared.id)",
                        "page" : "\(lastCharacters)"
                    ]
                    print(params)
                    NetworkManager.getChatApi(params: params, delegate: self, showHUD: false)
                }
            }else{
                refreshControl.endRefreshing()
            }
        }
        
        @IBAction func sendTapped(_ sender: Any) {
            if messageTF.text! != "" {
                let params = [
                    "to":"\(jobModel.user.id)",
                    "from":"\(Users.shared.id)",
                    "message":"\(messageTF.text!)"
                ]
                check = false
                NetworkManager.sendChatApi(params: params, delegate: self, showHUD: false)
            }else{
                Utilities.showAlert(title: "Error", message: "Type something to send.")
            }
        }
        
    }


extension CustomerChatController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ChatModel.shared.messageData.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let chat = MessageModel.init(dic: ChatModel.shared.messageData.reversed()[indexPath.row] as! [String : Any])
        if Users.shared.id != chat.from
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "providerCell", for: indexPath) as! ProviderCell
            cell.pMsg.text = chat.message
            cell.pBackView.layer.cornerRadius = 10
            if let time = Int(chat.msg_time) {
                cell.pMsgTime.text = "\(Utilities.UnixToTime(unixTime: time))"
            }
            cell.pName.text = "\(chat.from_user.first_name) \(chat.from_user.last_name)".capitalized
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "customerCell", for: indexPath) as! CustomerCell
            cell.cMsg.text = chat.message
            cell.cBackView.layer.cornerRadius = 10
            if let time = Int(chat.msg_time) {
                cell.cMsgTime.text = "\(Utilities.UnixToTime(unixTime: time))"
            }
            cell.cNAme.text = "You"
            return cell
        }
    }
    
}




extension CustomerChatController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiSendChat {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String : Any])
            self.messageTF.text! = ""
            let params = [
                "user1":"\(jobModel.user.id)",
                "user2":"\(Users.shared.id)"
            ]
            NetworkManager.getChatApi(params: params, delegate: self, showHUD: true)
        }
        if api == .eApiGetChat {
            refreshControl.endRefreshing()
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String : Any])
//            let data = dataArray as! [String:Any]
            let chat = ChatModel.init(dic: dataArray as! [String:Any])
            if check == false {
                ChatModel.shared.messageData.removeAll()
                ChatModel.shared.messageData.append(contentsOf: chat.data)
                self.chatTV.scroll(to: .bottom, animated: true)
            }else{
                ChatModel.shared.messageData.append(contentsOf: chat.data)
                self.chatTV.scroll(to: .top, animated: true)
            }
            
            self.chatTV.reloadData()
            paginateData = chat.next_page_url
        }
        if api == .eApiPaginate {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String : Any])
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiSendChat {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiGetChat {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
