//
//  SignInViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/7/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import FBSDKLoginKit
import TwitterKit
import SwiftLocation
import CoreLocation

class SignInViewController: UIViewController{
    
    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    @IBOutlet weak var facebookButton: UIButton!
    @IBOutlet weak var twitterButton: UIButton!

    
       var user: Users!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        self.navigationController?.isNavigationBarHidden = true
        getLocation()
        setupUI()
        if Users.shared.isLoggedIn() && Users.shared.license_img != "" {
            SocketManagerSingeleton.shared.connectToSocket()
            self.performSegue(withIdentifier: "signInSuccess", sender: self)
        }
    }
    
    func setupUI(){
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        emailField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
        passwordField.setValue(UIColor.white, forKeyPath: "placeholderLabel.textColor")
    }
    
    func getLocation() {
        LocationManager.shared.locateFromGPS(.oneShot, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
            switch result {
            case .success(let location):
                debugPrint("Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
                Extra.shared.tempLatitude = location.coordinate.latitude
                Extra.shared.tempLongitude = location.coordinate.longitude
            case .failure(let error):
                debugPrint("Received error: \(error)")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "verifyPhone" {
            let destination = segue.destination as! VerifyPhoneVC
            destination.delegate = self
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
    //MARK:- VALIDATORS
    func checkFields() -> Bool
    {
        if(self.emailField.text == "" || self.passwordField.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please check fields. All fields are mandatory")
            return false
        }
        else if((self.passwordField.text?.count)! < 6)
        {
            self.displayAlertWithOk(title: "Error", message: "Password cannot be of less than 6 characters.")
            return false
        }else if(Utilities.isValidEmail(testStr: self.emailField.text!) == false)
        {
            self.displayAlertWithOk(title: "Error", message: "Check Email format.")
            return false
        }
        
        return true
    }
    
    @IBAction func signInPressed(_ sender: Any) {
        if(checkFields())
        {
            let json: [String:Any] = ["email":"\(emailField.text!)","password":"\(passwordField.text!)","fcm_token":"\(APP_Delegate.deviceTokenString)"]
            self.callSignInAPI(json: json)
        }
    }
    
    @IBAction func forgotPassButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "goToForgetPassword", sender: self)
    }
    
    @IBAction func fbLoginPressed(_ sender: Any) {
        
        let readPermissions = ["email","public_profile"]
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: readPermissions, from: self)  { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    // if user cancel the login
                }
                else
                {
                    if (FBSDKAccessToken.current() != nil) {
                        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id,name,email, picture.type(large)"]).start(completionHandler: { (icon, data, err) in
                            if err != nil {
                                print("Graph Request Error............................\(err!)")
                                return
                            }else{
                                
                                let params = data as! [String:Any]
                                let id = params["id"] as! String
                                let name = params["name"] as! String
                                guard let email = params["email"] as? String else{
                                    Utilities.showAlert(title: "Error", message: "You are not registed with email on facebook. Use facebook account with a registered email or register manually.")
                                    return
                                }
                                let pic = params["picture"] as! [String:Any]
                                let picData = pic["data"] as! [String:Any]
                                let picURL = picData["url"] as! String
                                
                                
                                let names = name.split(separator: " ")
                                let userParams = [
                                    "first_name":"\(names.first!)",
                                    "last_name":"\(names.last!)",
                                    "username": "\(names.first!)\(names.last!)\(id)",
                                    "email":"\(email)",
                                    "radius":"\(Extra.shared.tempRadius)",
                                    "address": "empty",
                                    "avatar_file":"\(picURL)",
                                    "social_id":"\(id)",
                                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                                    "lat":"\(Extra.shared.tempLatitude)",
                                    "lng":"\(Extra.shared.tempLongitude)"
                                ]
                                
                                fbLoginManager.logOut()
                                NetworkManager.socialLoginApi(params: userParams, delegate: self, showHUD: true)
                            }
                        })
                    }
                }
            } else {
                print(error?.localizedDescription ?? "Sign up View Controller -> fbLoginPressed")
            }
        }
    }
    
    @IBAction func twitterLoginPressed(_ sender: Any) {
        
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let currentUser = TWTRAPIClient.withCurrentUser()
                
                var userParams: [String:Any] = [
                    "username": "\(session!.userName)\(session!.userID)",
                    "radius":"\(Extra.shared.tempRadius)",
                    "address": "empty",
                    "social_id":"\(session!.userID)",
                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                    "lat":"\(Extra.shared.tempLatitude)",
                    "lng":"\(Extra.shared.tempLongitude)"
                ]
                
                currentUser.requestEmail { (email, error) in
                    userParams["email"] = email!
                }
                currentUser.loadUser(withID: session!.userID, completion: { (user, error) in
                    let names = user!.name.split(separator: " ")
                    userParams["first_name"] = names.first
                    userParams["last_name"] = names.last
                    userParams["avatar_file"] = user!.profileImageLargeURL
                    
                })
                
                DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
                    NetworkManager.socialLoginApi(params: userParams, delegate: self, showHUD: true)
                })
            } else {
                print(error!.localizedDescription)
            }
        })
    }
    
    @IBAction func skipTapped(_ sender: Any) {
        
        Users.shared.isUserSkipLogin = true
        let userObj = [
            "id": "0",
            "username": "",
            "email": "xyz@gmail.com",
            "phone": "+13037898765",
            "address": "Appartment 9, South Avenue, NY, USA",
            "first_name": "",
            "last_name": "",
            "status": "",
            "type": "1",
            "rating": "0.0",
            "radius": "0",
            "user_id":"0",
            "job_status": "",
            "withdraw_status": "",
            "total_earning": "",
            "weekly_earning": "",
            "overall_rating": ""
        ]
        Users.shared.saveUser(user: Users.init(dic: userObj))
        NotificationCenter.default.post(.init(name: .displayNameUpdated))
        self.performSegue(withIdentifier: "signInSuccess", sender: self)
    }
    
    //MARK:- NETWORK CALL
    func callSignInAPI(json: [String:Any])
    {
        NetworkManager.loginApi(params: json, delegate: self, showHUD: true)
    }
    
}

extension SignInViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiLogin{
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String: Any])
            Users.shared.isUserSkipLogin = false
            UserDefaults.standard.set(passwordField.text!, forKey: My_Pass)
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            let user = Users.init(dic: providerDetails)
            Users.shared.saveUser(user: user)
            SocketManagerSingeleton.shared.connectToSocket()
            Extra.shared.userDefaultDataSave(user: user, providerDetails: providerDetails)
            NotificationCenter.default.post(.init(name: .displayNameUpdated))
            self.performSegue(withIdentifier: "signInSuccess", sender: self)
            
        }else if api == .eApiSocialLogin {
            
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            Utilities.printPrettyJSONFromDic(params: providerDetails)
            let user = Users.init(dic: providerDetails)
            Users.shared.saveUser(user: user)
            Extra.shared.userDefaultDataSave(user: user, providerDetails: providerDetails)
            
            if user.phone == ""{
                self.user = user
                self.performSegue(withIdentifier: "verifyPhone", sender: self)
            }else if user.license_img == "" {
                Extra.shared.tempLoginCheck = true
                self.performSegue(withIdentifier: "showLicenseVerify", sender: self)
            }else{
                SocketManagerSingeleton.shared.connectToSocket()
                self.performSegue(withIdentifier: "signInSuccess", sender: self)
            }
            
            
        }else if api == .eApiUpdatePhone {
            if Users.shared.license_img == "" {
                Extra.shared.tempLoginCheck = true
                self.performSegue(withIdentifier: "showLicenseVerify", sender: self)
            }else{
                SocketManagerSingeleton.shared.connectToSocket()
                self.performSegue(withIdentifier: "signInSuccess", sender: self)
            }
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiLogin{
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiSocialLogin {
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdatePhone{
            self.displayAlertWithOk(title: "Error", message: error.localizedDescription)
        }
    }
}

extension SignInViewController: VerifyPhoneDelegate {
    
    func sendPhoneVerifyResponse(_ phone: String?, completion: (() -> ())?) {
        
        let params = [
            "user_id":"\(self.user.id)",
            "fcm_token":"\(APP_Delegate.deviceTokenString)",
            "phone":"\(phone!)"
        ]
        NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
    
    }
    
}
