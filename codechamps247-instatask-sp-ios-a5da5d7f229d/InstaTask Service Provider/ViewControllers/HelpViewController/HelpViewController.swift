//
//  HelpViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/27/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit

class HelpViewController: UIViewController {

    @IBOutlet weak var issueName: UITextField!
    @IBOutlet weak var issueType: UITextField!
    @IBOutlet weak var issueDescription: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        self.setupUI()
    }
    
    func setupUI()
    {
        whiteTopTitle(title: "Help")
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
     
    func checkFields() -> Bool
    {
        if(self.issueName.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter title for your issue.")
            return false
        }
        else if(self.issueType.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter your issue type.")
            return false
        }else if(self.issueDescription.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Description can't be empty.")
            return false
        }
        
        return true
    }
    
    @IBAction func submitRequest(_ sender: Any) {
        if Users.shared.isUserSkipLogin == false {
            if checkFields() {
                let params = [
                    "user_id":"\(Users.shared.id)",
                    "user_type":"1",
                    "issue_name":"\(issueName.text!)",
                    "issue_type":"\(issueType.text!)",
                    "issue_description":"\(issueDescription.text!)",
                    "status": "Satisfied"
                ]
                NetworkManager.creatIssueApi(params: params, delegate: self, showHUD: true)
            }
        }else{
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
    }

}

extension HelpViewController: RedirectDelegate {
    func redirectTo() {
        SocketManagerSingeleton.shared.disconectSocket()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension HelpViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiCreatIssue {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            let alertController = UIAlertController(title: "Successful!", message: "Your issue has been received. Our support center will contact you shortly.", preferredStyle: .alert)
            let alertAction = UIAlertAction(title: "OK", style: .default) { (alert) in
                self.navigationController?.popViewController(animated: true)
            }
            alertController.addAction(alertAction)
            self.present(alertController, animated: true, completion: nil)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiCreatIssue {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
