//
//  EditRequestVC.swift
//  InstaTask Service Provider
//
//  Created by Arqam Butt on 8/19/19.
//  Copyright © 2019 Mustafa Shaheen. All rights reserved.
//

import UIKit

class EditRequestVC: UIViewController , NetworkManagerDelegate{

    @IBOutlet weak var cancelBtn: UIButton!
    @IBOutlet weak var confirmBtn: UIButton!
    
    var jobID = Int()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        cancelBtn.layer.borderColor = mainBlue.cgColor
        cancelBtn.layer.borderWidth = 1.0
        cancelBtn.layer.cornerRadius = 6
        confirmBtn.layer.cornerRadius = 6
        
    }
    
    @IBAction func onclick_Confirm(_ sender: Any) {
        let params = [
            "job_id":"\(jobID)",
            "status":"1"
        ]
        NetworkManager.jobEditRequestApi(params: params, delegate: self, showHUD: true)
    }

    @IBAction func onclick_Cancel(_ sender: Any) {
        let params = [
            "job_id":"\(jobID)",
            "status":"0"
        ]
        NetworkManager.jobEditRequestApi(params: params, delegate: self, showHUD: true)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiJobEditRequest {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            self.dismiss(animated: true, completion: nil)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        Utilities.showAlert(title: "Error", message: error.localizedDescription)
    }
}
