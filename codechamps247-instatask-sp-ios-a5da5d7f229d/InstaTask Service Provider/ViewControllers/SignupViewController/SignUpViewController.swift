//
//  SignUpViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import iOS_Slide_Menu
import FBSDKLoginKit
import TwitterKit

class SignUpViewController: UIViewController {
    
//    CustomerLoginCell
    @IBOutlet weak var tableView: UITableView!
    
    var params = [String:Any]()
    var check = false
    var user:Users!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    

    func setupUI()
    {
        self.navigationController?.isNavigationBarHidden = true
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 325
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "CustomerLoginCell", bundle: nil), forCellReuseIdentifier: "CustomerLoginCell")
        
        tableView.reloadData()
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "verificationPhone" {
            let destination = segue.destination as! VerifyPhoneVC
            destination.delegate = self
            self.navigationController?.isNavigationBarHidden = false
        }
    }
    
//    @objc func signUpTapped(){
//        self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
//    }

}

extension SignUpViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change in case of influencer and business owner
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        return 710
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "CustomerLoginCell") as! CustomerLoginCell
        
        cell.delegate = self
        cell.selectionStyle = .none
        
        return cell
    }
    
}

extension SignUpViewController: SignUpDelegate
{
    func selectedSignIn() {
        self.navigationController?.popViewController(animated: true)
    }
    
    func signUpPressed(params: [String:Any]) {
        self.params = params
        check = false
//        loginWithPhone()
        self.performSegue(withIdentifier: "verificationPhone", sender: self)
    }
    
    func facebookPressed() {
    
        let readPermissions = ["email","public_profile"]
        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
        fbLoginManager.logIn(withReadPermissions: readPermissions, from: self)  { (result, error) -> Void in
            if (error == nil){
                if (result?.isCancelled)!{
                    // if user cancel the login
                }
                else
                {
                    if (FBSDKAccessToken.current() != nil) {
                        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id,name,email, picture.type(large)"]).start(completionHandler: { (icon, data, err) in
                            if err != nil {
                                print("Graph Request Error............................\(err!)")
                                return
                            }else{
                                
                                let params = data as! [String:Any]
                                let id = params["id"] as! String
                                let name = params["name"] as! String
                                guard let email = params["email"] as? String else{
                                    Utilities.showAlert(title: "Error", message: "You are not registed with email on facebook. Use facebook account with a registered email or register manually.")
                                    return
                                }
                                let pic = params["picture"] as! [String:Any]
                                let picData = pic["data"] as! [String:Any]
                                let picURL = picData["url"] as! String
                                
                                
                                let names = name.split(separator: " ")
                                let userParams = [
                                    "first_name":"\(names.first!)",
                                    "last_name":"\(names.last!)",
                                    "username": "\(names.first!)\(names.last!)\(id)",
                                    "email":"\(email)",
                                    "radius":"\(Extra.shared.tempRadius)",
                                    "address": "empty",
                                    "avatar_file":"\(picURL)",
                                    "social_id":"\(id)",
                                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                                    "lat":"\(Extra.shared.tempLatitude)",
                                    "lng":"\(Extra.shared.tempLongitude)"
                                ]
                                
                                fbLoginManager.logOut()
                                self.callSignUpAPI(json: userParams)
                            }
                        })
                    }
                }
            } else {
                print(error?.localizedDescription ?? "Sign up View Controller -> fbLoginPressed")
            }
        }
        
//        let readPermissions = ["email"]
//        let fbLoginManager : FBSDKLoginManager = FBSDKLoginManager()
//        fbLoginManager.logIn(withReadPermissions: readPermissions, from: self)  { (result, error) -> Void in
//            if (error == nil){
////                let fbloginresult : FBSDKLoginManagerLoginResult = result!
//                if (result?.isCancelled)!{
//                    // if user cancel the login
//                }
//                else{
//                    DispatchQueue.main.async {
//                        let json: [String:AnyObject] = ["fbAccessToken": result?.token.tokenString as AnyObject]
//                        self.callSignUpAPI(json: json)
//                    }
//                }
//            } else {
//                print(error?.localizedDescription ?? "Sign up View Controller -> facebookPressed")
//            }
//        }
    }
    
    func twitterPressed() {
        
        TWTRTwitter.sharedInstance().logIn(completion: { (session, error) in
            if (session != nil) {
                let currentUser = TWTRAPIClient.withCurrentUser()
                
                var userParams: [String:Any] = [
                    "username": "\(session!.userName)\(session!.userID)",
                    "radius":"\(Extra.shared.tempRadius)",
                    "address": "empty",
                    "social_id":"\(session!.userID)",
                    "fcm_token":"\(APP_Delegate.deviceTokenString)",
                    "lat":"\(Extra.shared.tempLatitude)",
                    "lng":"\(Extra.shared.tempLongitude)"
                ]
                
                currentUser.requestEmail { (email, error) in
                    if let ema = email {
                        userParams["email"] = ema
                    }
                    if let err = error {
                        Utilities.showAlert(title: "Error", message: "\(err)")
                        return
                    }
                }
                currentUser.loadUser(withID: session!.userID, completion: { (user, error) in
                    let names = user!.name.split(separator: " ")
                    userParams["first_name"] = names.first
                    userParams["last_name"] = names.last
                    userParams["avatar_file"] = user!.profileImageLargeURL
                    
                })
                
                self.callSignUpAPI(json: userParams)
                
            } else {
                print(error!.localizedDescription)
            }
        })
        
    }
    
    func showAlert(title: String, message: String) {
        self.displayAlertWithOk(title: title, message: message)
    }
    
    //MARK:- NETWORK CALL
    func callSignUpAPI(json: [String:Any])
    {
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0, execute: {
            NetworkManager.socialLoginApi(params: json, delegate: self, showHUD: true)
        })
        //self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
//        NetworkManager.register(parameters: json) { (result) in
//            DispatchQueue.main.async {
//                Utilities.hideProgressLoader(view: self.view)
//                if(result == "success")
//                {
//                    self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
//                }
//                else{
//                    self.displayAlertWithOk(title: "Error", message: result!)
//                }
//            }
//        }
    }
    
}

extension SignUpViewController: VerifyPhoneDelegate {

    func sendPhoneVerifyResponse(_ phone: String?, completion: (() -> ())?) {
        
        self.params["phone"] = "\(phone!)"
        print("Params with phones", self.params)

        Extra.shared.tempParams = self.params
        Extra.shared.tempLoginCheck = false
        if self.check == true {
            let params = [
                "user_id":"\(Users.shared.id)",
                "fcm_token":"\(APP_Delegate.deviceTokenString)",
                "phone":"\(phone!)"
            ]
            NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
        }else{
            self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
        }
    }
    
}

//extension SignUpViewController: AKFViewControllerDelegate
//{
//    func prepareLoginViewController(loginViewController: AKFViewController) {
//        loginViewController.delegate = self
//        //UI Theming - Optional
//        loginViewController.uiManager = SkinManager(skinType: .classic, primaryColor: mainBlue)
//    }
//
//    func loginWithPhone(){
//        let inputState = UUID().uuidString
//        let vc = (accountKit?.viewControllerForPhoneLogin(with: nil, state: inputState))!
//        vc.isSendToFacebookEnabled = true
//        vc.delegate = self
//        self.prepareLoginViewController(loginViewController: vc)
//        self.present(vc as UIViewController, animated: true, completion: nil)
//    }
//
//    //    func viewController(_ viewController: (UIViewController & AKFViewController)!, didCompleteLoginWithAuthorizationCode code: String!, state: String!) {
//    //        if(accountKit != nil)
//    //        {
//    //            accountKit.requestAccount {
//    //                (account, error) -> Void in
//    //                if let phoneNumber = account?.phoneNumber{
//    //                    DispatchQueue.main.async {
//    //                        let userPhone = "+" + phoneNumber.countryCode + phoneNumber.phoneNumber
//    //                        Utilities.showProgressLoader(view: self.view)
//    //                        let params: [String:AnyObject] = ["name": self.user!.name as AnyObject, "password": self.user!.pass as AnyObject, "email": self.user!.email as AnyObject, "phoneNumber":userPhone as AnyObject]
//    //                        NetworkManager.register(parameters: params) { (result) in
//    //                            DispatchQueue.main.async {
//    //                                Utilities.hideProgressLoader(view: self.view)
//    //                                if(result == "success")
//    //                                {
//    //                                    self.performSegue(withIdentifier: "signUpSuccess", sender: self)
//    //                                }
//    //                                else{
//    //                                    self.displayAlertWithOk(title: "Error", message: result!)
//    //                                }
//    //                            }
//    //                        }
//    //                    }
//    //                }
//    //                else
//    //                {
//    //                    print(error)
//    //                }
//    //            }
//    //        }
//    //    }
//
//    func viewController(_ viewController: (UIViewController & AKFViewController), didCompleteLoginWith accessToken: AKFAccessToken, state: String) {
//        if(accountKit != nil)
//        {
//            accountKit.requestAccount {
//                (account, error) -> Void in
//                if let phoneNumber = account?.phoneNumber{
//                    DispatchQueue.main.async {
//                        let userPhone = "+" + phoneNumber.countryCode + phoneNumber.phoneNumber
//
//                        self.params["phone"] = "\(userPhone)"
//                        print("Params with phones", self.params)
//
//                        Extra.shared.tempParams = self.params
//                        Extra.shared.tempLoginCheck = false
//                        if self.check == true {
//                            let params = [
//                                "user_id":"\(Users.shared.id)",
//                                "fcm_token":"\(APP_Delegate.deviceTokenString)",
//                                "phone":"\(userPhone)"
//                            ]
//                            NetworkManager.updatePhoneApi(params: params, delegate: self, showHUD: true)
//                        }else{
//                            self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
//                        }
//
//
////                                }
////                                else{
////                                    self.displayAlertWithOk(title: "Error", message: result!)
////                                }
////                            }
////                        }
//                    }
//                }
//                else
//                {
//                    print(error ?? "Sign up View Controller")
//                }
//            }
//        }
//    }
//
//    func viewController(_ viewController: (UIViewController & AKFViewController), didFailWithError error: Error) {
//        // ... implement appropriate error handling ...
//        print("\(String(describing: viewController)) did fail with error: \(error.localizedDescription)")
//    }
//
//    func viewControllerDidCancel(_ viewController: (UIViewController & AKFViewController)) {
//        // ... handle user cancellation of the login process ...
//    }
//}



extension SignUpViewController: NetworkManagerDelegate {

    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        
        if api == .eApiSocialLogin {
            
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            Utilities.printPrettyJSONFromDic(params: providerDetails)
            let user = Users.init(dic: providerDetails)
            if user.phone == "" {
                self.user = user
                check = true
//                loginWithPhone()
                self.performSegue(withIdentifier: "verificationPhone", sender: self)
            }else{
                check = false
                Users.shared.saveUser(user: user)
                self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
            }
            
        }else if api == .eApiUpdatePhone {
            Users.shared.saveUser(user: user)
            self.performSegue(withIdentifier: "showLicenseVerification", sender: self)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiSocialLogin {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiUpdatePhone {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}

