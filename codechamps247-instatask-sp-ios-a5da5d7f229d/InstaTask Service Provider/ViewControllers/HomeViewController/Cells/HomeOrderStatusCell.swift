//
//  HomeOrderStatusCell.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 12/9/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import GoogleMaps
import HCSStarRatingView

class HomeOrderStatusCell: UITableViewCell {
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var minsAwayLabel: UILabel!
    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var providerNameLabel: UILabel!
    @IBOutlet weak var ratingLabelView: UILabel!
    @IBOutlet weak var starRatingView: HCSStarRatingView!
    @IBOutlet weak var shareButton: UIButton!
    @IBOutlet weak var contactButton: UIButton!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var beforeImageView: UIImageView!
    @IBOutlet weak var afterImageView: UIImageView!
    @IBOutlet weak var approveButton: UIButton!
    @IBOutlet weak var disapproveButton: UIButton!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        avatarImageView.layer.cornerRadius = avatarImageView.frame.size.width / 2
        avatarImageView.clipsToBounds = true
        avatarImageView.image = UIImage.init(named: "placeholder-profile")
        
        approveButton.layer.cornerRadius = 8
        contactButton.layer.cornerRadius = 8
        shareButton.layer.cornerRadius = 8
        disapproveButton.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
