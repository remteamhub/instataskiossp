//
//  HomeViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//  Added two origins.

import UIKit
import iOS_Slide_Menu
import GoogleMaps
import GooglePlaces
import HCSStarRatingView
//import Stripe
import SwiftLocation
import Kingfisher
import SocketIO

class HomeViewController: UIViewController, SlideNavigationControllerDelegate {
    
    @IBOutlet weak var mapView: UIView!
    @IBOutlet weak var pendingBackView: UIView!
    @IBOutlet weak var pendingMshLbl: UILabel!
    
    //DEFAULT BOTTOM VIEW
    @IBOutlet weak var lastJobView: UIView!
    
    //JOB REQUEST VIEW
    @IBOutlet weak var jobRequestView: UIView!
    @IBOutlet weak var confirmJobButton: UIButton!
    @IBOutlet weak var cancelJobButton: UIButton!
    @IBOutlet weak var confirmJobAvatarView: UIImageView!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var locationAddress: UILabel!
    @IBOutlet weak var serviceName: UILabel!
    @IBOutlet weak var servicePrice: UILabel!
    
    //ARRVIAL CONFIRMATION VIEW
    @IBOutlet weak var confirmArrivalView: UIView!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var confirmArrivalButton: UIButton!
    @IBOutlet weak var confirmArrivalLocation: UILabel!
    @IBOutlet weak var confirmArrivalLane: UILabel!
    
    //JOB STARTED BOTTOM VIEW
    @IBOutlet weak var jobStartedView: UIView!
    @IBOutlet weak var jobStartedMessage: UIButton!
    @IBOutlet weak var jobStartedButton: UIButton!
    @IBOutlet weak var jobStartedLocation: UILabel!
    @IBOutlet weak var jobStartedLane: UILabel!
    @IBOutlet weak var jobStartedImage: UIImageView!
    @IBOutlet weak var msgBannerView: UIView!
    @IBOutlet weak var bannerMsgLbl: UILabel!
    
    //MODE FOR JOB STARTED
    var jobStarted = false
    
    //BOTTOM REVIEW VIEW
    @IBOutlet weak var reviewView: UIView!
    @IBOutlet weak var submitReviewButton: UIButton!
    @IBOutlet weak var reviewAvatarView: UIImageView!
    @IBOutlet weak var reviewCustomerName: UILabel!
    @IBOutlet weak var reviewPrice: UILabel!
    @IBOutlet weak var reviewRating: HCSStarRatingView!
    
    //LEAVE FOR JOB VIEW
    @IBOutlet weak var leaveForJobView: UIView!
    @IBOutlet weak var leaveForJobCustomerImg: UIImageView!
    @IBOutlet weak var LeaveForJobCustomerName: UILabel!
    @IBOutlet weak var LeaveForJobCustomerLocation: UILabel!
    @IBOutlet weak var LeaveForJobLane: UILabel!
    @IBOutlet weak var LeaveForJobEstTime: UILabel!
    @IBOutlet weak var LeaveForJobPrice: UILabel!
    @IBOutlet weak var LeaveForJobBtn: UIButton!
    
    
    //LAST JOB VIEW
    @IBOutlet weak var lastJobPrice: UILabel!
    @IBOutlet weak var lastJobLane: UILabel!
    @IBOutlet weak var lastJobTime: UILabel!
    @IBOutlet weak var todayTotalPrice: UILabel!
    @IBOutlet weak var todayTotalHours: UILabel!
    @IBOutlet weak var todayTotalTrip: UILabel!
    
    //NAV BAR ONLINE STATUS
    var onlineStatus: UILabel!
    var onlineSwitch: UISwitch!
    
    let msg = "Account needs to be approved by admin before you recieve jobs."
    var fileURL:URL?
    //var markers:[GMSMarker] = []
    var req: LocationRequest?
    var rating = CGFloat()
    var timer : Timer?
    var jobCheck = false
    var currentState = CurrentState(rawValue: 0)
    var editJobREquest = false
    var pendingData = PendingJobModel()
    var zoom: Float = 16.5
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
//        SlideNavigationController.sharedInstance().enableShadow = false
//        let storyBoard: UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
//        let secondViewController = storyBoard.instantiateViewController(withIdentifier: "LeftMenuViewController") as! LeftMenuViewController
//        SlideNavigationController.sharedInstance().leftMenu = secondViewController
        
//        SlideNavigationController.sharedInstance()?.portraitSlideOffset
        setupUI()
        loadLocation()
        
        if Users.shared.isUserSkipLogin == false {
            userDefaultCheck { (status) in
                if status == Extra.shared.checkApprovalStatus(status: .approve) {
                    pendingBackView.isHidden = true
                }else{
                    pendingBackView.isHidden = false
                }
            }
        
        }else{
            self.lastJobLane.text! = "No Service"
            self.lastJobPrice.text! = "$0"
            self.lastJobTime.text! = "0:00:00"
            self.todayTotalTrip.text! = "0"
            self.todayTotalHours.text! = "0"
            self.todayTotalPrice.text! = "$0"
            pendingBackView.isHidden = true
        }
        
        if let online_status = UserDefaults.standard.value(forKey: ONLINE_STATUS) as? Int {
            if online_status == Extra.shared.checkJobStatus(status: .signedout){
                onlineStatus.text = "Offline"
                onlineSwitch.setOn(false, animated: true)
            }else{
                onlineStatus.text = "Online"
                onlineSwitch.setOn(true, animated: true)
            }
        }
        
//        dispatchSocketGroup.enter()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            SocketManagerSingeleton.shared.setupSocketManager(delegate: self)
//        }
        
        if Users.shared.isUserSkipLogin == false {
            dispatchGroup.enter()
            NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(Users.shared.id)", "type":"1"], delegate: self, showHUD: true)
            
            dispatchGroup.enter()
            NetworkManager.limitationsApi(delegate: self, showHUD: false)
        }
        
    }
    
    
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 1.0) {
//            //self.showJobToAccept()
//
//        }
        if Users.shared.isUserSkipLogin == false {
            dispatchGroup.enter()
            NetworkManager.updateLatLongApi(params: ["lat" : "\(Extra.shared.tempLatitude)", "lng" : "\(Extra.shared.tempLongitude)", "fcm_token" : "\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: false)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.navigationController?.isNavigationBarHidden = false
    }

    func userDefaultCheck(handler: (Int)->()) {
        if let status = UserDefaults.standard.value(forKey: approvalStatus) as? Int{
            return handler(status)
        }
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: "Home")
        self.navigationController?.navigationBar.tintColor = .white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        setupLeftButton()
        observeNotifications()
        self.resetViews()
        
        //Setting Up Pending  Message
        let height = msg.height(withConstrainedWidth: self.view.frame.width, font: UIFont.systemFont(ofSize: 11))
        pendingBackView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: height + 8)
        pendingMshLbl.text = msg
        
        //setting up Top Right Button
        onlineStatus = UILabel.init(frame: CGRect.init(x: 0, y: 0, width: 50, height: 30))
        onlineStatus.textColor = .white
        onlineStatus.font = Theme.primaryFontWithSize().withSize(11.0)
        onlineStatus.text = "Offline"
        onlineStatus.textAlignment = .right
        
        onlineSwitch = UISwitch.init(frame: CGRect.init(x: 55, y: 0, width: 35, height: 20))
        onlineSwitch.transform = CGAffineTransform(scaleX: 0.75, y: 0.75)
        onlineSwitch.addTarget(self, action: #selector(changedOnlineStatus), for: .valueChanged)
        let switchView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 90, height: 30))
        
        switchView.addSubview(onlineStatus)
        switchView.addSubview(onlineSwitch)
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem.init(customView: switchView)
        
        cancelJobButton.layer.borderColor = mainBlue.cgColor
        cancelJobButton.layer.borderWidth = 1.0
        cancelJobButton.layer.cornerRadius = 6
        confirmJobButton.layer.cornerRadius = 6
        
        messageButton.layer.borderColor = mainBlue.cgColor
        messageButton.layer.borderWidth = 1.0
        messageButton.layer.cornerRadius = 6
        confirmArrivalButton.layer.cornerRadius = 6
        
        jobStartedMessage.layer.borderColor = mainBlue.cgColor
        jobStartedMessage.layer.borderWidth = 1.0
        jobStartedMessage.layer.cornerRadius = 6
        jobStartedButton.layer.cornerRadius = 6
        
        LeaveForJobBtn.layer.cornerRadius = 6
        
        
        submitReviewButton.layer.cornerRadius = 6
        
        //setting button actions
        confirmJobButton.addTarget(self, action: #selector(confirmJobBtnTapped), for: .touchUpInside)
        confirmArrivalButton.addTarget(self, action: #selector(confirmArrivalBtnTapped), for: .touchUpInside)
        jobStartedButton.addTarget(self, action: #selector(jobStartedBtnTapped), for: .touchUpInside)
        submitReviewButton.addTarget(self, action: #selector(submitReviewTapped), for: .touchUpInside)
        cancelJobButton.addTarget(self, action: #selector(cancelJobBtnTapped), for: .touchUpInside)
        LeaveForJobBtn.addTarget(self, action: #selector(leaveForJobBtnTapped), for: .touchUpInside)
        jobStartedMessage.addTarget(self, action: #selector(contactCustomer), for: .touchUpInside)
        messageButton.addTarget(self, action: #selector(contactCustomer), for: .touchUpInside)
        
        confirmJobAvatarView.layer.cornerRadius = confirmJobAvatarView.frame.size.width / 2
        confirmJobAvatarView.clipsToBounds = true
        confirmJobAvatarView.image = UIImage.init(named: "placeholder-profile")
        
        leaveForJobCustomerImg.layer.cornerRadius = confirmJobAvatarView.frame.size.width / 2
        leaveForJobCustomerImg.clipsToBounds = true
        leaveForJobCustomerImg.image = UIImage.init(named: "placeholder-profile")
        
        reviewAvatarView.layer.cornerRadius = reviewAvatarView.frame.size.width / 2
        reviewAvatarView.clipsToBounds = true
        reviewAvatarView.image = UIImage.init(named: "placeholder-profile")
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        jobStartedImage.addGestureRecognizer(tapGestureRecognizer)
        
    }

    func setupLeftButton()
    {
        let sideMenuButton = UIImageView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        sideMenuButton.image = UIImage.init(named: "menu_icon")
        sideMenuButton.contentMode = .scaleAspectFill
        
        let myView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: 40, height: 40))
        let tapGesture = UITapGestureRecognizer.init(target: self, action: #selector(self.openMenu))
        myView.addGestureRecognizer(tapGesture)
        myView.isUserInteractionEnabled = true
        myView.addSubview(sideMenuButton)
        let item2 = UIBarButtonItem(customView: myView)
//        self.navigationItem.setLeftBarButtonItems([item2], animated: true)
        
        SlideNavigationController.sharedInstance()?.leftBarButtonItem = item2
    }
    
    func observeNotifications() {
        NotificationCenter.default.addObserver(self, selector: #selector(openNewMessage), name: .openSpNewMessage, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openProfile), name: .openProfile , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openMyRequests), name: .openMyRequest , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openEarning), name: .openEarnings , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openHelp), name: .openHelp , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(logOut), name: .logout , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openPendingJobs), name: .pendingJobs, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRemoteNotification), name: .openRemoteNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openJobIdNotification), name: .jobByIdNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openUserStatusNotification), name: .userStatusNotification , object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(reinstateBackgroundTask), name: UIApplication.didBecomeActiveNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openRatingUI), name: .openRatingUI, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(openDismissJobNotification), name: .dismissJobNotification , object: nil)
         NotificationCenter.default.addObserver(self, selector: #selector(openEditJobView), name: .openEditJobNotification , object: nil)
    }
    
    
    func removeObservers(){
        NotificationCenter.default.removeObserver(self, name: .openSpNewMessage, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openProfile, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openMyRequest, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openEarnings, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openHelp, object: nil)
        NotificationCenter.default.removeObserver(self, name: .logout, object: nil)
        NotificationCenter.default.removeObserver(self, name: .pendingJobs, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openRemoteNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .jobByIdNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .userStatusNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .dismissJobNotification, object: nil)
        NotificationCenter.default.removeObserver(self, name: .openEditJobNotification, object: nil)
        NotificationCenter.default.removeObserver(LeftMenuViewController(), name: .displayNameUpdated, object: nil)
    }
    
    //MARK:- LOCATION WORK
    func loadLocation()
    {
        // Manually request given authorization
        //LocationManager.shared.requireUserAuthorization(.whenInUse)//Locator.requestAuthorizationIfNeeded(.whenInUse)
        getLocation()
        getContinuousLocation()
        let res = LocationManager.shared.onAuthorizationChange.add { newStatus in
            print("Authorization status changed to \(newStatus)")
            switch newStatus {
            case .available:
                self.getLocation()
            case .undetermined:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> undetermined <********")
            case .denied:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> denied <********")
            case .restricted:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> restricted <********")
            case .disabled:
                LocationManager.shared.onAuthorizationChange.removeAll()
                print("********> disabled <********")
            }
        }
        print("Locator Result: \n", res.magnitude)
    }
    
    
    func getLocation() {
        LocationManager.shared.locateFromGPS(.oneShot, accuracy: .block, timeout: Timeout.Mode.absolute(3000)) { (result) in
            switch result {
            case .success(let location):
                debugPrint("Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
                Extra.shared.tempLatitude = location.coordinate.latitude; Extra.shared.tempLongitude = location.coordinate.longitude;
                let camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: self.zoom)
                Extra.shared.vi_Map = GMSMapView.map(withFrame: self.mapView.frame, camera: camera)
                self.mapView.addSubview(Extra.shared.vi_Map!)
                self.updateMarker(lat: location.coordinate.latitude, long: location.coordinate.longitude, bool: false)
            case .failure(let error):
                debugPrint("Received error: \(error)")
            }
        }
        
    }
    
    func getContinuousLocation() {
         req = LocationManager.shared.locateFromGPS(.continous, accuracy: .block, distance: .some(CLLocationDistance(2.0)), activity: .automotiveNavigation, timeout: Timeout.Mode.absolute(10.0)) { (result) in
            switch result {
            case .success(let location):
                print("Location received: \(location.coordinate.latitude), \(location.coordinate.longitude)")
            Extra.shared.tempLatitude = location.coordinate.latitude
            Extra.shared.tempLongitude = location.coordinate.longitude
                if Users.shared.isUserSkipLogin == false {
                    let coords = [
                        "lat":"\(Extra.shared.tempLatitude)",
                        "lng":"\(Extra.shared.tempLongitude)"
                    ]
                    SocketManagerSingeleton.shared.getSocket()?.emit("getDriversData", coords)
                }
            case .failure(let error):
                print("Received error: \(error)")
            }
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        //showChatFromHome
        if (segue.identifier == "showChatFromHome") {
            let destination = segue.destination as! CustomerChatController
            //set history item here
            destination.jobModel = self.pendingData
        }
    }
    
    func slideNavigationControllerShouldDisplayLeftMenu() -> Bool {
        return true
    }
    
    //MARK:- LISTENER FUNCTIONS
    @objc func logOut()
    {
        if Users.shared.isUserSkipLogin == false {
            NetworkManager.logoutApi(params: ["fcm_token":"\(APP_Delegate.deviceTokenString)"], delegate: self, showHUD: true)
        }else {
            Users.shared.deleteUser {
                Extra.shared.UserDefaultDataRemove()
                self.removeObservers()
                onlineSwitch.isOn = false
                SocketManagerSingeleton.shared.disconectSocket()
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
    }
    
    @objc func openProfile()
    {
        self.performSegue(withIdentifier: "showProfile", sender: self)
    }
    
    @objc func openMyRequests()
    {
        self.performSegue(withIdentifier: "showMyRequests", sender: self)
    }
    
    @objc func openEarning()
    {
        self.performSegue(withIdentifier: "openEarningsSegue", sender: self)
    }

    @objc func openHelp()
    {
        self.performSegue(withIdentifier: "showHelpPage", sender: self)
    }
    
    @objc func openMenu()
    {
        SlideNavigationController.sharedInstance()?.toggleLeftMenu()
    }
    
    @objc func openPendingJobs()
    {
        self.performSegue(withIdentifier: "pendingJobs", sender: self)
    }
    
    @objc func contactCustomer(_ sender:Any){
        self.performSegue(withIdentifier: "showChatFromHome", sender: self)
    }
    
    @objc func openNewMessage() {
        if Extra.shared.alreadyOnChat != true && Extra.shared.userClickedNotification == true{
            Extra.shared.alreadyOnChat = true
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.performSegue(withIdentifier: "showChatFromHome", sender: self)
            }
        }
    }
    
    @objc func openUserStatusNotification() {
        if Extra.shared.checkApprovalStatus(status: .unApprove) == Int(Extra.shared.tempStatus) {
            let params = ["fcm_token":"\(APP_Delegate.deviceTokenString)","user_id":"\(Users.shared.id)","job_status": "3"]
            NetworkManager.updateWorkStatusApi(params: params, delegate: self, showHUD: true)
//            pendingBackView.isHidden = false
//            self.onlineStatus.text = "Offline"
//            self.displayAlert(title: "Alert", message: "License unapprove by admin.")
//            UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
//            status = AdminApprovalStatus(rawValue: 1)!
//            onlineSwitch.setOn(false, animated: true)
        }else{
            UserDefaults.standard.set(0, forKey: ONLINE_STATUS)
            UserDefaults.standard.set(0, forKey: approvalStatus)
            pendingBackView.isHidden = true
            if(onlineSwitch.isOn){
                UserDefaults.standard.set(0, forKey: ONLINE_STATUS)
                self.onlineStatus.text = "Online"
            }
            else{
                UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
                self.onlineStatus.text = "Offline"
            }
        }
    }
    
    @objc func openRatingUI(){
        print("\n *** opening Rating View *** \n")
        jobCheck = true
        NetworkManager.checkJobByIdApi(params: ["job_id" : "\(Extra.shared.tempCustomerJobID)"], delegate: self, showHUD: false)
    }
    
    @objc func openRemoteNotification(){
        print("Recieved Remote Notification <===============")
        if Extra.shared.checkNotification == "checkCurrentJob" {
            dispatchGroup.enter()
            NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(Users.shared.id)", "type":"1"], delegate: self, showHUD: false)
        }
    }
    
    @objc func openJobIdNotification(){
        jobCheck = false
        if Extra.shared.checkJobNotification == "checkJobById" {
            NetworkManager.checkJobByIdApi(params: ["job_id" : "\(Extra.shared.checkJobById)"], delegate: self, showHUD: false)
        }
    }
    
    @objc func openDismissJobNotification(){
        self.stopTimer()
        self.checkStateOfViews()
    }
    
    @objc func openEditJobView(){
        self.editJobREquest = true
        dispatchGroup.enter()
        NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(Users.shared.id)", "type":"1"], delegate: self, showHUD: false)
    }
    
    func checkStateOfViews(){
        if let acceptedJob = UserDefaults.standard.value(forKey: AcceptedJob) as? Int{
            print("got id here : ", acceptedJob)
            
            if Extra.shared.checkCurrentState(view: .lastJobView) == currentState {
                self.resetViews()
            }else if Extra.shared.checkCurrentState(view: .leaveforJobView) == currentState {
                self.showLeaveForJob()
            }else if Extra.shared.checkCurrentState(view: .confirmArrivalView) == currentState {
                self.showConfirmArrivalView()
            }else if Extra.shared.checkCurrentState(view: .jobRequestView) == currentState {
                self.showJobToAccept()
            }else if Extra.shared.checkCurrentState(view: .jobStartedView) == currentState {
                self.showJobStartedView()
            }
            
        }else{
            self.resetViews()
        }
    }
    //MARK:- ONLINE STATUS CHANGE FUNCTION
    @objc func changedOnlineStatus()
    {
        var params = ["fcm_token":"\(APP_Delegate.deviceTokenString)","user_id":"\(Users.shared.id)"]
        userDefaultCheck { (status) in
            switch status {
            case 1: //unapproved
                self.displayAlert(title: "Alert", message: msg)
                onlineSwitch.setOn(false, animated: true)
                break
            default:
                print("Success")
                if(onlineSwitch.isOn)
                {
                    params["job_status"] = "0"
                    NetworkManager.updateWorkStatusApi(params: params, delegate: self, showHUD: true)
                    self.onlineStatus.text = "Online"
                }
                else
                {
                    params["job_status"] = "3"
                    NetworkManager.updateWorkStatusApi(params: params, delegate: self, showHUD: true)
                    self.onlineStatus.text = "Offline"
                }
            }
        }
    }
    //MARK:- JOB SITUATION IMAGE
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let imagePickerController = UIImagePickerController()
        imagePickerController.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .camera
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            imagePickerController.sourceType = .photoLibrary
            self.present(imagePickerController, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        self.present(actionSheet, animated: true, completion: nil)
        
        actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
            return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
            }.first?.isActive = false
    }
    
    //MARK:- MAP UTILITY METHODS
    func updateMarker(lat: Double, long: Double, desLat: Double = 0.0, desLong: Double = 0.0, bool: Bool)
    {
        print("\n ~~~~~~ \(lat) \(long) ~~~~~~ \n")
        Extra.shared.myMarkersDictionary.removeAll()
        Extra.shared.vi_Map?.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: zoom)
        Extra.shared.vi_Map?.camera = camera
        
        // Creates a marker in the center of the map.
        
        Extra.shared.marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        Extra.shared.marker.icon = UIImage.init(named: "locationMarker")
        Extra.shared.marker.map =  Extra.shared.vi_Map
//        self.markers.append(Extra.shared.marker!)
        Extra.shared.myMarkersDictionary[Users.shared.stringID()] = Extra.shared.marker
        mapView.isUserInteractionEnabled = true
        
        if bool == true {
            let homeTruckIcon = GMSMarker()
            homeTruckIcon.icon = UIImage(named: "map-pin")!.resizedImageWithinRect(rectSize: CGSize(width: 40, height: 40))
            homeTruckIcon.position = CLLocationCoordinate2D(latitude:desLat, longitude:desLong)
            homeTruckIcon.map = Extra.shared.vi_Map
//            self.markers.append(homeTruckIcon)
            Extra.shared.myMarkersDictionary["truck"] = homeTruckIcon
            self.setUpCameraBounds()
        }
    }
    
    func setUpCameraBounds(){
        let myLocation: CLLocationCoordinate2D = Extra.shared.myMarkersDictionary.values.first!.position
        var bounds: GMSCoordinateBounds = GMSCoordinateBounds(coordinate: myLocation, coordinate: myLocation)
        for m in Extra.shared.myMarkersDictionary.values {
            bounds = bounds.includingCoordinate(m.position)
        }
        let update = GMSCameraUpdate.fit(bounds, withPadding: CGFloat(170.0))
        Extra.shared.vi_Map?.animate(with: update)
    }

    
    //MARK:- BOTTOM VIEW METHODS
    func hideAllViews(){
        lastJobView.alpha = 0.0
        jobRequestView.alpha = 0.0
        confirmArrivalView.alpha = 0.0
        jobStartedView.alpha = 0.0
        reviewView.alpha = 0.0
        leaveForJobView.alpha = 0.0
        jobStartedImage.image = UIImage.init(named: "home-noImageAdded")
    }
    
    func resetViews(){
        hideAllViews()
        currentState = .lastJobView
        lastJobView.alpha = 1.0
    }
    
    func showJobToAccept()
    {
        hideAllViews()
        
        jobRequestView.alpha = 1.0
    }
    
    func hideJobToAccept()
    {
        jobRequestView.alpha = 0.0
    }
    
    func showConfirmArrivalView()
    {
        hideAllViews()
        currentState = .confirmArrivalView
        confirmArrivalView.alpha = 1.0
    }
    
    func showJobStartedView()
    {
        hideAllViews()
        currentState = .jobStartedView
        jobStartedView.alpha = 1.0
    }
    
    func showReviewView()
    {
        hideAllViews()
        
        reviewView.alpha = 1.0
    }
    
    func showLeaveForJob()
    {
        hideAllViews()
        currentState = .leaveforJobView
        leaveForJobView.alpha = 1.0
    }
    
    //MARK:- BOTTOM VIEWS BUTTONS METHODS
    @objc func messageBtnTapped()
    {
        //show messages view
        
    }
    
    @objc func confirmJobBtnTapped()
    {
        //Hit Accept Api
        NetworkManager.acceptJobApi(params: ["job_id":"\(Extra.shared.checkJobById)", "driver_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
    }
    
    @objc func cancelJobBtnTapped(){
        let params = [
            "job_id":"\(Extra.shared.checkJobById)",
            "provider_id":"\(Users.shared.id)"
        ]
        print(params)
        
        NetworkManager.rejectJobRequestApi(params: ["job_id":"\(Extra.shared.checkJobById)", "provider_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
        //NetworkManager.cancelJobApi(params: ["job_id":"\(Extra.shared.checkJobById)"], delegate: self, showHUD: true)
        
    }
    
    @objc func leaveForJobBtnTapped(){
        //Hit Leave for Job Api
        let params = [
            "job_status":"1",
            "job_id":"\(Extra.shared.tempJobId)",
        ]
        NetworkManager.updateStatusApi(params: params, delegate: self, showHUD: true)
    }
    
    @objc func confirmArrivalBtnTapped()
    {
        let params = [
            "job_status":"7",
            "job_id":"\(Extra.shared.tempJobId)",
        ]
        NetworkManager.updateStatusApi(params: params, delegate: self, showHUD: true)
    }
    
    @objc func jobStartedBtnTapped()
    {
        if(jobStarted)
        {
            
            guard let file = fileURL else{ self.displayAlert(title: "Alert", message: "Image not selected."); return }
            let params = [
                "job_id":"\(Extra.shared.tempJobId)",
            ]
            print("Image With URL:- \(file)")
            NetworkManager.requestJobApprovalApi(params: params, file: file, imageKey: "after_work_img", delegate: self, showHUD: true)
        }
        else
        {
            //not started yet
            guard let file = fileURL else{ self.displayAlert(title: "Alert", message: "Image not selected."); return }
            let params = [
                "job_status":"5",
                "job_id":"\(Extra.shared.tempJobId)",
            ]
            print("Image With URL: \(file)")
            NetworkManager.updateStatusWihtImageApi(params: params, file: file, imageKey: "current_situation_img", delegate: self, showHUD: true)
        }
    }
    
    @objc func submitReviewTapped()
    {
//        Utilities.showHUD()
//        DispatchQueue.main.asyncAfter(deadline: .now() + 4.0, execute: {
//            Utilities.hideHUD()
//            self.resetViews()
//        })
        NetworkManager.customerRatingApi(params: ["job_id" : "\(Extra.shared.tempCustomerJobID)", "customer_rating":"\(rating)"], delegate: self, showHUD: true)
    }
    
    @IBAction func didValueChange(_ sender: HCSStarRatingView){
        print(String(format: "Changed rating to %.1f", sender.value))
        rating = sender.value
    }
    
    func getTimer() {
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(HomeViewController.updateTimer)), userInfo: nil, repeats: true)
        BackgroundTask.shared.registerBackgroundTask()
    }
    
    func stopTimer(){
        //Extra.shared.seconds = 15
        timer?.invalidate()
        timer = nil
        DispatchQueue.main.async {
            dispatchGroup.enter()
            NetworkManager.limitationsApi(delegate: self, showHUD: false)
        }
    }
    //
    @objc func updateTimer() {
        print("\n", Extra.shared.seconds)
        if Extra.shared.seconds < 1 {
            self.stopTimer()
            if BackgroundTask.shared.backgroundTask != .invalid {
                BackgroundTask.shared.endBackgroundTask()
            }
            self.hideJobToAccept()
            self.checkStateOfViews()
        }else{
            BackgroundTask.shared.backgroundStateCheck()
            Extra.shared.seconds -= 1
        }
    }
    //MARK: BackgroundTask
    @objc func reinstateBackgroundTask() {
        if timer != nil && BackgroundTask.shared.backgroundTask == .invalid {
            BackgroundTask.shared.registerBackgroundTask()
        }
    }
    
    
    //MARK: Extra Methods
    func populateJobByIdData(data job: JobByIdModel) {
        serviceName.text = job.service_name
        servicePrice.text = "$\(job.service_price)"
        locationAddress.text = job.location_address
        jobStartedLocation.text = job.location_address
        customerName.text = "\(job.user.first_name) \(job.user.last_name)"
        self.confirmJobAvatarView.kf.setImage(with: URL(string: imageBackURL+job.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
        getTimer()
        print("got timer")
        self.showJobToAccept()
    }
    
    func currentJobData(currentJob data: CurrentJobModel){
        
        LeaveForJobCustomerName.text = "\(data.user.first_name) \(data.user.last_name)"
        LeaveForJobPrice.text = "$\(data.service_price)"
        LeaveForJobCustomerLocation.text = data.location_address
        LeaveForJobLane.text = data.service_name
        Extra.shared.tempJobId = data.id
        leaveForJobCustomerImg.kf.setImage(with: URL(string: imageBackURL+data.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
        if let acceptedJob = UserDefaults.standard.value(forKey: AcceptedJob) as? Int{
            if acceptedJob != data.id {
                self.hideJobToAccept()
                return
            }else{
                print("\n if this scenario works. \n")
            }
        }else{
            UserDefaults.standard.set(data.id, forKey: AcceptedJob)
            self.showLeaveForJob()
        }
        
    }
    
    func currentJobUpdateStatus(currentJob data: CurrentJobModel){
        Extra.shared.tempJobId = data.id
        if data.job_status == Extra.shared.checkStatus(status: .leave_for_job) {
            confirmArrivalLocation.text = data.location_address
            confirmArrivalLane.text = data.service_name
            self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
            self.showConfirmArrivalView()
        }
        if data.job_status == Extra.shared.checkStatus(status: .confirmArrival) {
            jobStartedLocation.text = data.location_address
            jobStartedLane.text = data.service_name
            self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
            self.showJobStartedView()
        }
        if data.job_status == Extra.shared.checkStatus(status: .working) {
            self.jobStarted = true
            self.jobStartedMessage.alpha = 0.0
            self.fileURL = nil
            self.jobStartedImage.image = UIImage.init(named: "home-noImageAdded")
            self.jobStartedButton.setTitle("Job Completed", for: .normal)
            self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
            
        }
        if data.job_status == Extra.shared.checkStatus(status: .complete) {
            jobStarted = false
            jobStartedMessage.alpha = 0.0
            jobStartedButton.setTitle("Job Started", for: .normal)
            //UserDefaults.standard.removeObject(forKey: "approval")
            //Extra.shared.jobApproval = -1
            self.showReviewView()
        }
    }
}
//
////TABLEVIEW WORK
//extension HomeViewController : UITableViewDelegate, UITableViewDataSource {
//
//    func numberOfSections(in tableView: UITableView) -> Int {
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        //change in case of influencer and business owner
//        return 1
//    }
//
//    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
//    {
//        return 1079
//    }
//
//    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: "HomeOrderStatusCell") as! HomeOrderStatusCell
//
//        return cell
//    }
//
//}


extension HomeViewController: NetworkManagerDelegate {
    
//    var providerDetails = dataArr["providerDetails"] as! [String:Any]
//    providerDetails += provData
//    let user = Users.init(dic: providerDetails)
//    Users.shared.saveUser(user: user)

    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiLogout {
            Users.shared.deleteUser {
                Extra.shared.UserDefaultDataRemove()
                self.removeObservers()
                onlineSwitch.isOn = false
                SocketManagerSingeleton.shared.disconectSocket()
                self.navigationController?.isNavigationBarHidden = true
                self.navigationController?.popToRootViewController(animated: true)
            }
        }
            
        dispatchGroup.notify(queue: .main) {
            if api == .eApiCheckCurrentJobs {
                if let dataArr = dataArray as? NSDictionary {
                    
                    Utilities.printPrettyJSONFromDic(params: dataArr as! [String : Any])
                    
                    if let providerDetails = (dataArr["providerDetails"] as! [String:Any])["approval_status"] as? Int {
                        //providerDetails == 0 ? print("\n====>>> Approved <<<====\n") : print("\n====>>> Un Approved <<<====\n")
                        if Extra.shared.checkApprovalStatus(status: .unApprove) == providerDetails {
                            self.pendingBackView.isHidden = false
                            self.onlineStatus.text = "Offline"
                            if Extra.shared.firstTimeLogin != true {
                                self.displayAlert(title: "Alert", message: "License unapprove by admin.")
                            }
                            UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
                            UserDefaults.standard.set(1, forKey: approvalStatus)
                            self.onlineSwitch.setOn(false, animated: true)
                        }else{
                            UserDefaults.standard.set(0, forKey: ONLINE_STATUS)
                            UserDefaults.standard.set(0, forKey: approvalStatus)
                            self.pendingBackView.isHidden = true
                            if(self.onlineSwitch.isOn){
                                UserDefaults.standard.set(0, forKey: ONLINE_STATUS)
                                self.onlineStatus.text = "Online"
                            }
                            else{
                                UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
                                self.onlineStatus.text = "Offline"
                            }
                        }
                    }
                    
                    var data = CurrentJobModel()
                    
                    if let jobs = dataArr["job"] as? [String:Any] {
                        data = CurrentJobModel.init(dic: jobs)
                        self.pendingData = PendingJobModel.init(dic: jobs)
                    }else{
                        data = CurrentJobModel.init(dic: dataArr as! [String : Any])
                    }
                    
                    
                    let second = Int(Utilities.currentToUnix())! - data.providerTimeLogs.lastLoginTime
                    let currentSec = second + data.providerTimeLogs.todayWorkingHours
                    
                    self.lastJobLane.text! = data.lastJob.service_name == "" ? "No Service" : data.lastJob.service_name
                    self.lastJobPrice.text! = "$\(data.lastJob.service_price)"
                    self.lastJobTime.text! = data.lastJob.job_schedual_time == 0 ? "0:00:00" : Utilities.UnixToTime(unixTime: data.lastJob.job_schedual_time)
                    self.todayTotalTrip.text! = "\(data.todayJobCount)"
                    self.todayTotalHours.text! = data.providerTimeLogs.lastLoginTime == 0 ? "0" : "\((currentSec / 60) / 60)"
                    self.todayTotalPrice.text! = "$\(data.todayJobEarnings)"
                    
                    
                    print("\n ====>>> \(data.provider.lat), \(data.provider.lng) <<<====")
                    print("\n ====>>> \(data.user.lat), \(data.user.lng) <<<====")
                    
                    Extra.shared.tempJobId = data.id
                    Extra.shared.checkJobById = data.id
                    Extra.shared.tempCustomerName = "\(data.provider.first_name) \(data.provider.last_name)"
                    if data.job_status == 3 {
                        self.onlineSwitch.isUserInteractionEnabled = true
                    }else{
                        self.onlineSwitch.isUserInteractionEnabled = false
                    }
                    if data.job_status == Extra.shared.checkStatus(status: .pending) {
                        
                        self.serviceName.text = data.service_name
                        self.servicePrice.text = "$\(data.service_price)"
                        self.locationAddress.text = data.location_address
                        self.jobStartedLocation.text = data.location_address
                        self.customerName.text = "\(data.user.first_name) \(data.user.last_name)"
                        self.confirmJobAvatarView.kf.setImage(with: URL(string: imageBackURL+data.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
                        self.showJobToAccept()
                        
                    }else if data.job_status == Extra.shared.checkStatus(status: .accept) {
                        
                        Extra.shared.jobApproval = -1
                        self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        self.LeaveForJobCustomerName.text = "\(data.user.first_name) \(data.user.last_name)"
                        self.LeaveForJobPrice.text = "$\(data.service_price)"
                        self.LeaveForJobCustomerLocation.text = data.location_address
                        self.LeaveForJobLane.text = data.service_name
                        self.leaveForJobCustomerImg.kf.setImage(with: URL(string: imageBackURL+data.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
                        Extra.shared.tempJobId = data.id
                        print("\n >>>>>> \(data.lat) \(data.lng) <<<<<< \n")
                        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
                        self.showLeaveForJob()
                        
                    }else if data.job_status == Extra.shared.checkStatus(status: .cancel) {
                
                        self.bannerMsgLbl.text = "Arrived at location"
                        self.resetViews()
                        Extra.shared.jobApproval = -1
                        Extra.shared.tempJobId = 0
                        self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        UserDefaults.standard.removeObject(forKey: "approval")
                        UserDefaults.standard.removeObject(forKey: AcceptedJob)
                        self.updateMarker(lat: Extra.shared.tempLatitude, long: Extra.shared.tempLongitude, bool: false)
                        if data.user.fcm_token == "" || data.user.fcm_token != APP_Delegate.deviceTokenString {
                            Users.shared.deleteUser {
                                Extra.shared.UserDefaultDataRemove()
                                self.removeObservers()
                                SocketManagerSingeleton.shared.disconectSocket()
                                self.onlineSwitch.isOn = false
                                self.navigationController?.isNavigationBarHidden = true
                                self.navigationController?.popToRootViewController(animated: true)
                            }
                        }
                        
                    }else if data.job_status == Extra.shared.checkStatus(status: .leave_for_job) {
                        Extra.shared.jobApproval = -1
                        self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
                        self.confirmArrivalLocation.text = data.location_address
                        self.confirmArrivalLane.text = data.service_name
                        self.showConfirmArrivalView()
                    }else if data.job_status == Extra.shared.checkStatus(status: .working) {
                        self.jobStarted = true
                        self.jobStartedMessage.alpha = 0.0
                        self.fileURL = nil
                        self.jobStartedImage.image = UIImage.init(named: "home-noImageAdded")
                        self.jobStartedButton.setTitle("Job Completed", for: .normal)
                        self.jobStartedLocation.text = data.location_address
                        self.jobStartedLane.text = data.service_name
                        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
                        self.showJobStartedView()
                        if Extra.shared.jobApproval == 0 {
                            self.bannerMsgLbl.text = "Customer unapproved your job"
                            self.msgBannerView.backgroundColor = UIColor(red: 184, green: 47, blue: 59)
                        }else{
                            self.bannerMsgLbl.text = "Arrived at location"
                            self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        }
                    }else if data.job_status == Extra.shared.checkStatus(status: .complete){
                        //showRatingView(show: true)
                        self.resetViews()
                        UserDefaults.standard.removeObject(forKey: AcceptedJob)
                        //Extra.shared.jobApproval = -1
                        //                    orderByButton.alpha = 1.0
                        //                    sendButton.alpha = 1.0
                    }else if data.job_status == Extra.shared.checkStatus(status: .confirmArrival) {
                        
                        Extra.shared.jobApproval = -1
                        self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        self.jobStartedLocation.text = data.location_address
                        self.jobStartedLane.text = data.service_name
                        self.jobStarted = false
                        self.jobStartedMessage.alpha = 1.0
                        self.jobStartedImage.image = UIImage.init(named: "home-noImageAdded")
                        self.jobStartedButton.setTitle("Job Started", for: .normal)
                        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
                        self.bannerMsgLbl.text = "Arrived at location"
                        self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
                        self.showJobStartedView()
                        
                    }else if data.job_status == Extra.shared.checkStatus(status: .requestApproval) {
                        print("\n ***> Request Approval <*** \n")
                        self.bannerMsgLbl.text = "Requested Job Approval"
                        Extra.shared.jobApproval = 0
                        
                        self.jobStarted = true
                        self.jobStartedMessage.alpha = 0.0
                        self.fileURL = nil
                        self.jobStartedImage.kf.indicatorType = .activity
                        self.jobStartedImage.kf.setImage(with: URL(string:  imageBackURL+data.after_work_img))
                        self.jobStartedButton.setTitle("Job Completed", for: .normal)
                        self.jobStartedLocation.text = data.location_address
                        self.jobStartedLane.text = data.service_name
                        self.updateMarker(lat: data.lat, long: data.lng, desLat: data.provider.lat, desLong: data.provider.lng, bool: true)
                        self.showJobStartedView()
                        
                        //show request approval view
                    }
                    
                    if self.editJobREquest == true {
                        self.editJobREquest = false
                        let vc = self.storyboard?.instantiateViewController(withIdentifier: "EditRequestVC") as! EditRequestVC
                        vc.jobID = Extra.shared.tempJobId
                        self.present(vc, animated: true, completion: nil)
                    }
                    
                    if let cDispJob = data.currentDispatchedJob {
                        print("\n ~~~~~~~ (current Dispatched Job) ~~~~~~~ \n")
                        
                        Extra.shared.tempJobId = cDispJob.id
                        Extra.shared.checkJobById = cDispJob.id
                        
                        if cDispJob.id != 0 {
                            self.serviceName.text = cDispJob.service_name
                            self.servicePrice.text = "$\(cDispJob.service_price)"
                            self.locationAddress.text = cDispJob.location_address
                            self.jobStartedLocation.text = cDispJob.location_address
                            self.customerName.text = "\(cDispJob.user.first_name) \(cDispJob.user.last_name)"
                            self.confirmJobAvatarView.kf.setImage(with: URL(string: imageBackURL+cDispJob.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
                            self.getTimer()
                            print("got timer")
                            self.showJobToAccept()
                            return
                        }
                        
                    }
                }
            }else if api == .eApiUpdateLatLong {
                print("\n ***> Lat Long Updated <*** \n")
            }
        }
            
        
        if api == .eApiCheckJobById {
            let data = JobByIdModel.init(dic: dataArray as! [String:Any])
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            if jobCheck == false {
                self.populateJobByIdData(data: data)
            }else{
                reviewPrice.text = "$\(data.service_price)"
                reviewCustomerName.text = "\(data.user.first_name) \(data.user.last_name)"
                reviewAvatarView.kf.setImage(with: URL(string: imageBackURL+data.user.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
                self.showReviewView()
            }
        }else if api == .eApiUpdateWorkStatus {
            if self.onlineStatus.text == "Offline" {
               UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
            }else{
                UserDefaults.standard.set(0, forKey: ONLINE_STATUS)
            }
            if Extra.shared.checkApprovalStatus(status: .unApprove) == Int(Extra.shared.tempStatus) {
                pendingBackView.isHidden = false
                self.onlineStatus.text = "Offline"
                if Extra.shared.firstTimeLogin != true {
                    self.displayAlert(title: "Alert", message: "License unapprove by admin.")
                }
                UserDefaults.standard.set(3, forKey: ONLINE_STATUS)
                UserDefaults.standard.set(1, forKey: approvalStatus)
                onlineSwitch.setOn(false, animated: true)
            }
        }else if api == .eApiAcceptJob {
            self.stopTimer()
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            self.currentJobData(currentJob: CurrentJobModel.init(dic: dataArray as! [String:Any]))
        }else if api == .eApiRejectJobRequest {
            Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
            self.stopTimer()
            self.checkStateOfViews()
        }else if api == .eApiUpdateStatus {
            self.currentJobUpdateStatus(currentJob: CurrentJobModel.init(dic: dataArray as! [String:Any]))
        }else if api == .eApiRequestJobApproval{
            print("\n ***> Request Approval <*** \n") //1
            self.bannerMsgLbl.text = "Requested Job Approval"
            self.msgBannerView.backgroundColor = UIColor(red: 119, green: 176, blue: 59)
            Extra.shared.jobApproval = 0
        }else if api == .eApiLimitations {
            let resp = dataArray as! NSDictionary
            let limitationData = resp["data"] as! NSDictionary
            Extra.shared.seconds = limitationData["max_job_acception_time"] as! Int
            Extra.shared.maxJobAcceptance = limitationData["max_job_limit"] as! Int
        }else if api == .eApiCustomerRating {
            Extra.shared.tempJobId = 0
            dispatchGroup.enter()
            NetworkManager.checkCurrentJobsApi(params: ["user_id":"\(Users.shared.id)", "type":"1"], delegate: self, showHUD: true)
            self.updateMarker(lat: Extra.shared.tempLatitude, long: Extra.shared.tempLongitude, bool: false)
            self.resetViews()
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiLogout {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCheckCurrentJobs {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCheckJobById {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdateWorkStatus {
//            if Users.shared.job_status == Extra.shared.checkJobStatus(status: .free){
//                onlineSwitch.setOn(true, animated: true)
//            }else{
//                onlineSwitch.setOn(false, animated: true)
//            }
        }else if api == .eApiAcceptJob {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCancelJob {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdateStatus {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiRequestJobApproval{
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiLimitations {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiRejectJobRequest {
            self.stopTimer()
            self.checkStateOfViews()
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}

extension HomeViewController: SocketStatusDelegate {
    
    func socketEventStatus(event: SocketClientEvent, data: [Any], ack: SocketAckEmitter) {
        dispatchGroup.notify(queue: .main) {
            if event == .connect {
                print("\n \n connect: \n \n \(data) \(ack) \n \n")
            }
            if event == .disconnect {
                print("\n \n disconnect: \n \n \(data) \(ack) \n \n")
            }
            if event == .error {
                print("\n \n error: \n \n \(data) \(ack) \n \n")
            }
        }
    }
    
    func didReceiveResponseOfApi(api: SocketName, dataArray: Any) {
        dispatchGroup.notify(queue: .main) {
            if api == .status {
                print("\n  Status Data Received: \n", dataArray, "\n")
                let coords = [
                    "lat":"\(Extra.shared.tempLatitude)",
                    "lng":"\(Extra.shared.tempLongitude)"
                ]
                Utilities.printPrettyJSONFromDic(params: coords as [String:Any])
                SocketManagerSingeleton.shared.getSocket()?.emit("getDriversData", coords)
                //print("==>" ,SocketManagerSingeleton.shared.getSocket())
    //            SocketManagerSingeleton.shared.getSocket().emit("getDriversData", with: coords as! [Any])
            }
            if api == .getDriversData {
                print("\n Driver Data Received: \n", dataArray, "\n")
                
                if let data = dataArray as? NSArray {
                    if let datArr = data[0] as? NSArray {
                        if let dic = datArr[0] as? [String:Any] {
                            if let arrTwo = dic["resolve"] as? NSArray {
                                for j in arrTwo {
                                    let dataDic = j as! [String:Any]
                                    let id = dataDic["usr_id"] as! Int
                                    print(Extra.shared.myMarkersDictionary.count)
                                    for mark in Extra.shared.myMarkersDictionary {
                                       
                                        if "\(id)" == mark.key || mark.key == Users.shared.stringID() {
                                            print("Found Marker")
                                            print(mark.key)
                                            if let lat = dataDic["lat"] as? Double, let lng = dataDic["lng"] as? Double {
                                                CATransaction.begin()
                                                CATransaction.setAnimationDuration(1.0)
                                                print(lat, lng)
                                                mark.value.position = CLLocationCoordinate2D.init(latitude: lat, longitude: lng)
                                                
                                                print(Extra.shared.vi_Map)
                                                mark.value.map = Extra.shared.vi_Map
                                                CATransaction.commit()
                                            }
                                        }else{
                                            print("Did Not Found Marker")
                                            print(mark.key)
                                        }
                                    }
                                }
                            }
                        }
                    }
                    
                }
                
            }
        }
    }
}

extension HomeViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        
        if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[.editedImage] as? UIImage {
            selectedImage = editImage
        }
        
        picker.dismiss(animated: true, completion: nil)
        
        if let data = selectedImage!.jpegData(compressionQuality: 0.2){
            
            self.fileURL = Utilities.getDocumentsDirectory()
            if jobStarted {
                self.fileURL!.appendPathComponent("after_work_img")
            }else{
                self.fileURL!.appendPathComponent("current_situation_img.png")
            }
            
            if FileManager.default.fileExists(atPath: self.fileURL!.path){
                do{
                    try FileManager.default.removeItem(at: fileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: self.fileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            let provider = LocalFileImageDataProvider(fileURL: self.fileURL!)
            self.jobStartedImage.kf.setImage(with: provider, options: [.forceRefresh])
        }
    }
}
