//
//  ProfileViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import HCSStarRatingView
import Kingfisher

class ProfileViewController: UIViewController, ChangePasswordDelegate {
    
    @IBOutlet weak var profileImgTopView: UIView!
    @IBOutlet weak var profileImgView: UIImageView!
    
    @IBOutlet weak var updateBtn: UIButton!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var cameraButton: UIButton!
    @IBOutlet weak var ratingLabel: UILabel!
    @IBOutlet weak var ratingView: HCSStarRatingView!
    
    @IBOutlet weak var editBtn: UIButton!
    @IBOutlet weak var milesText: UILabel!
    @IBOutlet weak var radiusSlider: UISlider!
    @IBOutlet weak var contactLabel: UILabel!
    @IBOutlet weak var requestsLabel: UILabel!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var passwordLabel: UILabel!

    var fileURL:URL?
    var check = false
    var params = [String:Any]()
    override func viewDidLoad() {
        super.viewDidLoad()
        if Users.shared.social_id != "" {
            editBtn.isHidden = false
        }else{
            editBtn.isHidden = true
        }
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: "Profile")
        profileImgView.layer.cornerRadius = profileImgView.frame.size.width / 2
        profileImgView.clipsToBounds = true
        profileImgView.kf.indicatorType = .activity
        profileImgView.kf.setImage(with: URL(string: imageBackURL+Users.shared.avatar), placeholder: UIImage.init(named: "placeholder-profile"), options: [.transition(.fade(0.2))])
        
        cameraButton.layer.cornerRadius = cameraButton.frame.size.width / 2
        cameraButton.clipsToBounds = true
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "pencil-edit-button-64") , style: .plain, target: self, action: #selector(buttonTapped))
        
        profileImgView.backgroundColor = .lightGray
        
        self.disableEditing()
        updateBtn.layer.cornerRadius = 6
        
        radiusSlider.value = Float(Users.shared.radius)
        milesText.text = "\(Users.shared.radius) miles"
        nameTF.text = "\(Users.shared.first_name) \(Users.shared.last_name)"
        emailLabel.text = Users.shared.email
        contactLabel.text = Users.shared.phone
        ratingView.allowsHalfStars = true
        ratingView.value = CGFloat(round(100*Users.shared.rating)/100)
        ratingLabel.text = "\(round(100*Users.shared.rating)/100)/5.0"
    
//        let provider = LocalFileImageDataProvider(fileURL: Utilities.getDocumentsDirectory())
//        self.profileImgView.kf.setImage(with: provider, options: [.forceRefresh])
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    func disableEditing(){
        nameTF.isEnabled = false
        updateBtn.isHidden = true
    }
    
    func enableEditing(){
        nameTF.isEnabled = true
        updateBtn.isHidden = false
    }
    
    func checkEditingState(){
        if check == false {
            check = true
            self.enableEditing()
        }else{
            check = false
            self.disableEditing()
        }
    }
    
    @objc func buttonTapped(){
        print("Edit button Tapped")
        if Users.shared.isUserSkipLogin == false {
            self.checkEditingState()
        }else {
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
    }
    
    
    func didUpdateData(params: [String : Any]) {
        self.params = params
    }
    
    @IBAction func onClickUpdate(_ sender: Any) {
        
        let names = nameTF.text!.split(separator: " ")
        if !nameTF.text!.contains(" ") || names.last! == names.first! {
            self.displayAlert(title: "Error", message: "Please enter your full name.")
            return
        }
        if nameTF.text! == "" {
            self.displayAlert(title: "Error", message: "Name filed empty.")
            return
        }
        if (Users.shared.radius != Int(radiusSlider.value)) {
            params["radius"] = "\(Int(radiusSlider.value))"
        }else{
            params["radius"] = "\(Users.shared.radius)"
        }
        params["user_id"] = "\(Users.shared.id)"
        params["fcm_token"] = "\(APP_Delegate.deviceTokenString)"
        params["first_name"] = names.first!
        params["last_name"] = names.last!
        print(params)
        if let file = fileURL {
            NetworkManager.updateLicenseApi(params: params, file: file, insurance: nil, image: "avatar_file", delegate: self, showHUD: true)
        }else{
            NetworkManager.updateLicenseApi(params: params, file: nil, insurance: nil, image: "avatar_file", delegate: self, showHUD: true)
        }
        
    }
    
    @IBAction func sliderValue(_ sender: UISlider) {
        
        milesText.text = "\(Int(sender.value)) miles"
        
    }
    
    //MARK:- BUTTON ACTIONS
    @IBAction func camerBtnPressed(_ sender: Any) {
        
        if Users.shared.isUserSkipLogin == false {
            
            let imagePickerController = UIImagePickerController()
            imagePickerController.delegate = self
            
            let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
            
            actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
                imagePickerController.sourceType = .camera
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
                imagePickerController.sourceType = .photoLibrary
                self.present(imagePickerController, animated: true, completion: nil)
            }))
            
            actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            self.present(actionSheet, animated: true, completion: nil)
            
            actionSheet.view.subviews.flatMap({$0.constraints}).filter{ (one: NSLayoutConstraint)-> (Bool)  in
                return (one.constant < 0) && (one.secondItem == nil) &&  (one.firstAttribute == .width)
                }.first?.isActive = false
            
        }else {
            Utilities.showAlert(title: "Alert!", message: "SignIn/Signup to use this feature.", delegate: self)
        }
        
        
    }
    
    
    @IBAction func editPasswordPressed(_ sender: Any) {
        self.enableEditing()
        check = true
        let vc = storyboard?.instantiateViewController(withIdentifier: "EditPasswordVC") as! EditPasswordVC
        vc.delegate = self
        self.present(vc, animated: true, completion: nil)
    }
    
}

extension ProfileViewController: RedirectDelegate {
    func redirectTo() {
        SocketManagerSingeleton.shared.disconectSocket()
        self.navigationController?.isNavigationBarHidden = true
        self.navigationController?.popToRootViewController(animated: true)
    }
}

extension ProfileViewController : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        var selectedImage: UIImage?
        
        if let originalImage = info[.originalImage] as? UIImage {
            selectedImage = originalImage
        }else if let editImage = info[.editedImage] as? UIImage {
            selectedImage = editImage
        }
        
        picker.dismiss(animated: true, completion: nil)
        
        if let data = selectedImage!.jpegData(compressionQuality: 0.5){
            
            self.fileURL = Utilities.getDocumentsDirectory()
            self.fileURL!.appendPathComponent("avatar_image.png")
            
            
            if FileManager.default.fileExists(atPath: self.fileURL!.path){
                do{
                    try FileManager.default.removeItem(at: fileURL!)
                    
                }catch{
                    print(error.localizedDescription)
                }
            }
            
            do{
                try data.write(to: self.fileURL!, options: .atomic)
            }catch{
                print(error.localizedDescription)
            }
            
            self.enableEditing()
            check = true
            
            let provider = LocalFileImageDataProvider(fileURL: self.fileURL!)
            self.profileImgView.kf.setImage(with: provider, options: [.forceRefresh])
        }
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    
}


extension ProfileViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiUpdateLicense {
            
            let resp = dataArray as! [String:Any]
            let userProfile = resp["userProfile"] as! [String:Any]
            var providerDetails = resp["ProviderDetails"] as! [String:Any]
            providerDetails += userProfile
            Utilities.printPrettyJSONFromDic(params: providerDetails)
            let user = Users.init(dic: providerDetails)
            Users.shared.saveUser(user: user)
            NotificationCenter.default.post(.init(name: .displayNameUpdated))
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiUpdateLicense {
            self.displayAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
