//
//  MyEarningsViewController.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/16/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//

import UIKit

class MyEarningsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    
    var withdraw = WithdrawModel()
    override func viewDidLoad() {
        super.viewDidLoad()
        if Users.shared.isUserSkipLogin == false {
            dispatchGroup.enter()
            NetworkManager.getBankInfoApi(params: ["user_id" : "\(Users.shared.id)"], delegate: self, showHUD: true)
            // Do any additional setup after loading the view.
            dispatchGroup.enter()
            NetworkManager.weeklyEarningApi(params: ["user_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
        }
        setupUI()
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: "My Earnings")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 149
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        
        tableView.bounces = false
        
        tableView.register(UINib(nibName: "EarningCell", bundle: nil), forCellReuseIdentifier: "EarningCell")
        
        tableView.reloadData()
    }

    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "showBankInfo"{
            let destination = segue.destination as! BankInformationViewController
            destination.object = self.withdraw
        }
    }
    
    func convertDateFormater(_ date: String) -> String
    {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd"
        let date = dateFormatter.date(from: date)
        dateFormatter.dateFormat = "dd-MM-yyyy"
        return  dateFormatter.string(from: date!)

    }

}

extension MyEarningsViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change for history and scheduled
        if(section == 0)
        {
            return 1
        }
        return WeeklyEarningModel.shared.weeklyEarningArray.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if indexPath.section == 0
        {
            return 149
        }
        return 75
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(indexPath.section == 0){
            let cell = tableView.dequeueReusableCell(withIdentifier: "EarningsHeaderCell") as! EarningsHeaderCell
            
            cell.selectionStyle = .none
            return cell
        }
        else
        {
            let cell = tableView.dequeueReusableCell(withIdentifier: "EarningCell") as! EarningCell
            let weeklyData = WeeklyEarningModel.init(dic: WeeklyEarningModel.shared.weeklyEarningArray[indexPath.row] as! [String:Any])
            
            cell.selectionStyle = .none
            cell.datesLbl.text = convertDateFormater(weeklyData.dates.start)  + " - " + convertDateFormater(weeklyData.dates.end)
            cell.priceLbl.text = "$\(weeklyData.amount)"
            cell.jobCount.text = "\(weeklyData.job_count) Jobs"
            
            return cell
        }
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if indexPath.section == 0
        {
            self.performSegue(withIdentifier: "showBankInfo", sender: self)
        }
    }
    
}


extension MyEarningsViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        dispatchGroup.notify(queue: .main) {
            if api == .eApiGetBankInfo {
                Utilities.printPrettyJSONFromDic(params: dataArray as! [String:Any])
                
                if let dataArr = dataArray as? [String:Any] {
                    let code = dataArr["code"] as! Int
                    if code == 200 {
                        let res = WithdrawModel.init(dic: dataArr["data"] as! [String:Any])
                        self.withdraw = res
                    }else{
                        Utilities.showAlert(title: "Error", message: dataArr["error_msg"] as! String)
                    }
                }
                
            }
            if api == .eApiWeeklyEarning {
                if let data = dataArray as? NSArray {
                    WeeklyEarningModel.shared.weeklyEarningArray.append(contentsOf: data)
                    self.tableView.reloadData()
                }
            }
        }
        
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiGetBankInfo {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
        if api == .eApiWeeklyEarning {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
