//
//  BankInformationViewController.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/16/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//

import UIKit

class BankInformationViewController: UIViewController {

    @IBOutlet weak var withdrawButton: UIButton!
    @IBOutlet weak var bankName: UITextField!
    @IBOutlet weak var swiftCode: UITextField!
    @IBOutlet weak var iban: UITextField!
    @IBOutlet weak var accNumber: UITextField!
    @IBOutlet weak var withdrawAmount: UITextField!
    @IBOutlet weak var country: UITextField!
    
    var object = WithdrawModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        setupUI()
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: "Bank Information")
        print(object.bank_name)
        bankName.text! = object.bank_name != "" ? object.bank_name.capitalized : ""
        swiftCode.text! = object.swift_code != "" ? object.swift_code.capitalized : ""
        iban.text! = object.IBAN != "" ? object.IBAN.capitalized : ""
        accNumber.text! = object.account_no != "" ? object.account_no.capitalized : ""
        country.text! = object.country != "" ? object.country.capitalized : ""
        
        withdrawButton.layer.cornerRadius = 8
    }
    
    func checkFields() -> Bool
    {
        if(self.bankName.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter Bank Name.")
            return false
        }
        else if(self.swiftCode.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter Swift Code.")
            return false
        }else if(self.iban.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter IBAN.")
            return false
        }else if(self.accNumber.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter Account Number.")
            return false
        }else if(self.withdrawAmount.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter Withdraw Amount.")
            return false
        }else if(self.country.text == "")
        {
            self.displayAlertWithOk(title: "Error", message: "Please enter Country.")
            return false
        }
        
        return true
    }
    
    
    @IBAction func pressedWithdrawButton(_ sender: Any) {
        if checkFields() {
            let params = [
                "user_id":"\(Users.shared.id)",
                "bank_name":"\(bankName.text!)",
                "swift_code":"\(swiftCode.text!)",
                "IBAN":"\(iban.text!)",
                "account_no":"\(accNumber.text!)",
                "country":"\(country.text!)",
                "withdraw_amount":"\(withdrawAmount.text!)"
            ]
            NetworkManager.addBankInfoApi(params: params, delegate: self, showHUD: true)
        }
        
    }
    
}


extension BankInformationViewController: NetworkManagerDelegate {
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiAddBankInfo {
             self.performSegue(withIdentifier: "showWithdrawConfirmation", sender: self)
        }
    }
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiAddBankInfo {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
}
