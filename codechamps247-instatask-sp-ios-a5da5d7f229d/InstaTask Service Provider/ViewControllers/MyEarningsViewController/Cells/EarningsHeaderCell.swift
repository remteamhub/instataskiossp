//
//  EarningsHeaderCell.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/16/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//

import UIKit

class EarningsHeaderCell: UITableViewCell {

    @IBOutlet weak var totalAmount: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
