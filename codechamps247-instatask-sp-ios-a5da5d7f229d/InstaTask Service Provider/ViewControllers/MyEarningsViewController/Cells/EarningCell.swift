//
//  EarningCell.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/16/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//

import UIKit

class EarningCell: UITableViewCell {

    @IBOutlet weak var datesLbl: UILabel!
    @IBOutlet weak var priceLbl: UILabel!
    @IBOutlet weak var jobCount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
