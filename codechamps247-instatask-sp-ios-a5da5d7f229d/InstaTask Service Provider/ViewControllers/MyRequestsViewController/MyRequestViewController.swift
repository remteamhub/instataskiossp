//
//  MyRequestViewController.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import UIKit
import HMSegmentedControl

class MyRequestViewController: UIViewController {
    
    @IBOutlet weak var topSegmentControl: HMSegmentedControl!
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var emptyLbl: UILabel!
    
    var mode = 0 //0 for scheduled and 1 for history
    var data = PreviousJobModel()
    var history: [String] = ["","",""]
    var scheduled: [String] = ["",""]
    var selectedIndex: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        if Users.shared.isUserSkipLogin == false {
            NetworkManager.checkPendingJobsApi(params: ["type" : "1", "user_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
        }
        setupUI()
    }
    
    func setupUI()
    {
        self.whiteTopTitle(title: "My Requests")
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        
        if Users.shared.isUserSkipLogin == false {
            emptyLbl.isHidden = true
            emptyLbl.text = ""
        }else{
            emptyLbl.isHidden = false
            emptyLbl.text = "SignIn/Signup to use this feature."
        }
        
        topSegmentControl.sectionTitles = ["Selected", "History"]
        topSegmentControl.selectionIndicatorColor = mainBlue
        topSegmentControl.selectionStyle = .fullWidthStripe
        topSegmentControl.selectionIndicatorLocation = .down
        topSegmentControl.selectedTitleTextAttributes = [NSAttributedString.Key.foregroundColor: mainBlue]
        
        topSegmentControl.addTarget(self, action: #selector(segmentedControlChangedValue), for: UIControl.Event.valueChanged)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.estimatedRowHeight = 605
        tableView.rowHeight = UITableView.automaticDimension
        tableView.tableFooterView = UIView()
        tableView.bounces = false
        
        tableView.register(UINib(nibName: "ScheduledJobCell", bundle: nil), forCellReuseIdentifier: "ScheduledJobCell")
        tableView.register(UINib(nibName: "HistoryCell", bundle: nil), forCellReuseIdentifier: "HistoryCell")
        
        //tableView.reloadData()
    }
    

    //MARK:- SEGMENT CONTROL CHANGED
    @objc func segmentedControlChangedValue(segment : HMSegmentedControl) {
        if Users.shared.isUserSkipLogin == false {
            emptyLbl.isHidden = true
            emptyLbl.text = ""
            self.mode = segment.selectedSegmentIndex
            if segment.selectedSegmentIndex == 0 {
                NetworkManager.checkPendingJobsApi(params: ["type" : "1", "user_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
            }else{
                NetworkManager.checkPreviousJobsApi(params: ["type" : "1", "user_id":"\(Users.shared.id)"], delegate: self, showHUD: true)
            }
        }else{
            emptyLbl.isHidden = false
            emptyLbl.text = "SignIn/Signup to use this feature."
        }
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation */
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if(segue.identifier == "openRequestDetail")
        {
            let destination = segue.destination as! RequestDetailViewController
            //set history item here
            destination.jobDetails = self.data
        }
    }
    
    @objc func contactProviderPressed()
    {
        self.performSegue(withIdentifier: "showChatFromHistory", sender: self)
    }

}

extension MyRequestViewController : UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //change for history and scheduled
        if(mode == 0)
        {
            return PendingJobModel.shared.pendingData.count
        }
        else if(mode == 1)
        {
            return PreviousJobModel.shared.previousData.count
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat
    {
        if(mode == 0)
        {
            return 322
        }
        else if(mode == 1)
        {
            return 201
        }
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell
    {
        if(mode == 0) // scheduled
        {
            return getCellForScheduled(indexPath: indexPath)
        }
        else if(mode == 1) // history
        {
            return getCellForHistory(indexPath: indexPath)
        }
        
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if(mode == 1) //only allow selection of history
        {
            selectedIndex = indexPath.row
            let data = PreviousJobModel.init(dic: PreviousJobModel.shared.previousData.reversed()[indexPath.row] as! [String:Any])
            self.data = data
            self.performSegue(withIdentifier: "openRequestDetail", sender: self)
        }
    }
    
    //MARK:- TABLEVIEW UTILITY METHODS
    
    func getCellForScheduled(indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ScheduledJobCell") as! ScheduledJobCell
        let data = PendingJobModel.init(dic: PendingJobModel.shared.pendingData[indexPath.row] as! [String:Any])
        cell.updateMarker(lat: data.lat, long: data.lng)
        cell.serviceType.text! = data.service_name
        cell.location.text! = data.location_address
        cell.customerName.text! = "\(data.user.first_name) \(data.user.last_name)"
        cell.price.text! = "$\(data.service_price)"
        cell.selectionStyle = .none
        cell.delegate = self
//        if let acceptedJob = UserDefaults.standard.value(forKey: AcceptedJob) as? Int {
//            if acceptedJob == data.id {
//                cell.startJobButton.isUserInteractionEnabled = true
//            }else{
//                cell.startJobButton.isUserInteractionEnabled = false
//            }
//        }
        return cell
    }
    
    func getCellForHistory(indexPath: IndexPath) -> UITableViewCell
    {
        let cell = tableView.dequeueReusableCell(withIdentifier: "HistoryCell") as! HistoryCell
        let data = PreviousJobModel.init(dic: PreviousJobModel.shared.previousData.reversed()[indexPath.row] as! [String:Any])
        cell.updateMarker(lat: data.lat, long: data.lng)
        cell.costLabel.text = "$\(data.service_price)"
        cell.dateTimeLabel.text = "\(Utilities.UnixToCurrentTime(unixTime: data.job_schedual_time))"
        cell.userTypeLabel.text = "\(data.provider.first_name) \(data.provider.last_name)"
        cell.rating.value = CGFloat(data.customer_rating)
        cell.selectionStyle = .none
        return cell
    }
    
    func currentJobUpdateStatus(currentJob data: CurrentJobModel){
        UserDefaults.standard.set(data.id, forKey: AcceptedJob)
        self.navigationController?.popViewController(animated: true)
    }
}

extension MyRequestViewController: NetworkManagerDelegate {
    
    func didReceiveResponseOfApi(api: ApiName, dataArray: Any) {
        if api == .eApiCheckPendingJobs {
            
            if let dataArr = dataArray as? NSDictionary {
                PendingJobModel.shared.pendingData.removeAll()
                let jobData = dataArr["job"] as! NSArray
                PendingJobModel.shared.pendingData.append(contentsOf: jobData)
            }
            if PendingJobModel.shared.pendingData.count == 0 {
                emptyLbl.isHidden = false
                emptyLbl.text = "No Job"
            }else{
                emptyLbl.isHidden = true
            }
            tableView.reloadData()
            
        }else if api == .eApiUpdateStatus {
            self.currentJobUpdateStatus(currentJob: CurrentJobModel.init(dic: dataArray as! [String:Any]))
        }else if api == .eApiCheckPreviousJobs{
            
            if let dataArr = dataArray as? NSArray {
                PreviousJobModel.shared.previousData.removeAll()
                PreviousJobModel.shared.previousData.append(contentsOf: dataArr)
            }
            if PreviousJobModel.shared.previousData.count == 0 {
                emptyLbl.isHidden = false
                emptyLbl.text = "No History"
            }else{
                emptyLbl.isHidden = true
            }
            tableView.reloadData()
        }
    }
    
    func didReceiveResponseOfApi(api: ApiName, error: NSError) {
        if api == .eApiCheckPendingJobs {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiUpdateStatus {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }else if api == .eApiCheckPreviousJobs {
            Utilities.showAlert(title: "Error", message: error.localizedDescription)
        }
    }
    
}

extension MyRequestViewController: ScheduledCellDelegate {
    func didTap(_ cell: ScheduledJobCell) {
        let indexPath = self.tableView.indexPath(for: cell)
        let data = PendingJobModel.init(dic: PendingJobModel.shared.pendingData[indexPath!.row] as! [String:Any])
        let params = [
            "job_status":"1",
            "job_id":"\(data.id)",
        ]
        NetworkManager.updateStatusApi(params: params, delegate: self, showHUD: true)
    }
}
