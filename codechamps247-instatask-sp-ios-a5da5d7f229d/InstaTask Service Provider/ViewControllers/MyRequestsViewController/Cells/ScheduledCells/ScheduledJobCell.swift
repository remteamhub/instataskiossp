//
//  ScheduledJobCell.swift
//  InstaTask Service Provider
//
//  Created by Asfand Shabbir on 12/16/18.
//  Copyright © 2018 Mustafa Shaheen. All rights reserved.
//


protocol ScheduledCellDelegate:class {
    func didTap(_ cell: ScheduledJobCell)
}


import UIKit
import GoogleMaps

class ScheduledJobCell: UITableViewCell {
    
    @IBOutlet weak var mapView: GMSMapView!
    
    @IBOutlet weak var startJobButton: UIButton!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var serviceType: UILabel!
    @IBOutlet weak var dateAndTime: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var location: UILabel!
    
    weak var delegate: ScheduledCellDelegate?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        startJobButton.layer.cornerRadius = 8
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateMarker(lat: Double, long: Double)
    {
        mapView.clear()
        let camera = GMSCameraPosition.camera(withLatitude: lat, longitude: long, zoom: 15.0)
        mapView.camera = camera

        // Creates a marker in the center of the map.
        let marker = GMSMarker()
        marker.position = CLLocationCoordinate2D(latitude: lat, longitude: long)
        marker.icon = UIImage.init(named: "locationMarker")
        marker.map = mapView
        
        mapView.isUserInteractionEnabled = false
    }
    
    @IBAction func buttonPressed(_ sender: Any) {
        delegate?.didTap(self)
    }
}
