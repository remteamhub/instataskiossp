//
//  ExtImageView.swift
//  SnikPic
//
//  Created by Aqsa Arshad on 13/11/2018.
//  Copyright © 2018 Aqsa Arshad. All rights reserved.
//

import Foundation
import UIKit
import Alamofire

extension UIImageView {
    
    func setImage (urlString: String,placeHolderString : String = "placeholder") {
        let placeHolderImage = UIImage.init(named: placeHolderString)
        
        if(urlString.trim() == "") {
            self.image = placeHolderImage
            return
        }
        
        self.kf.setImage(with: URL(string: urlString)!, placeholder: placeHolderImage, options: nil, progressBlock: { (receivedSize, totalSize) -> () in
            
        }, completionHandler: { (image, error, cacheType, imageURL) -> () in
            DispatchQueue.main.async {
                if(image != nil) {
                    self.image = image
                }
                
            }
        }
        )
    }
}

extension UIImage {
    
    /// Returns a image that fills in newSize
    func resizedImage(newSize: CGSize) -> UIImage {
        // Guard newSize is different
        guard self.size != newSize else { return self }
        
        UIGraphicsBeginImageContextWithOptions(newSize, false, 0.0);
        self.draw(in: CGRect(x: 0, y: 0, width: newSize.width, height: newSize.height))
        let newImage: UIImage = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return newImage
    }
    
    /// Returns a resized image that fits in rectSize, keeping it's aspect ratio
    /// Note that the new image size is not rectSize, but within it.
    func resizedImageWithinRect(rectSize: CGSize) -> UIImage {
        let widthFactor = size.width / rectSize.width
        let heightFactor = size.height / rectSize.height
        
        var resizeFactor = widthFactor
        if size.height > size.width {
            resizeFactor = heightFactor
        }
        
        let newSize = CGSize(width: size.width/resizeFactor, height: size.height/resizeFactor)
        let resized = resizedImage(newSize: newSize)
        return resized
    }
    
}
