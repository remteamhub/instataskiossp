//
//  extNotification.swift
//  InstaTask
//
//  Created by Asfand Shabbir on 11/21/18.
//  Copyright © 2018 CodeChamps. All rights reserved.
//

import Foundation

extension Notification.Name {
    static let openHome = Notification.Name(kOpenHomeNotification)
    static let openProfile = Notification.Name(kOpenProfileNotification)
    static let openMyRequest = Notification.Name(kOpenMyRequestsNotification)
    static let openEarnings = Notification.Name(kOpenEarningNotification)
    static let openHelp = Notification.Name(kOpenHelpNotification)
    static let openSettings = Notification.Name(kOpenSettingsNotification)
    static let logout = Notification.Name(kLogoutNotification)
    static let pendingJobs = Notification.Name(kPendingJobsNotification)
    static let openRemoteNotification = Notification.Name(kOpenRemoteNotification)
    static let displayNameUpdated = Notification.Name(kDisplayNameUpdated)
    static let jobByIdNotification = Notification.Name(kOpenJobByIdNotification)
    static let userStatusNotification = Notification.Name(kOpenUserStatusNotification)
    static let openRatingUI = Notification.Name(kOpenRatingUI)
    static let dismissJobNotification = Notification.Name(kDismissJobNotification)
    static let openEditJobNotification = Notification.Name(kOpenEditJobNotification)
    static let refreshChat = Notification.Name(kRefreshChat)
    static let openSpNewMessage = Notification.Name(kOpenSpNewMessage)
}
